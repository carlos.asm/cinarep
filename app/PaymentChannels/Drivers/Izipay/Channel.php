<?php

namespace App\PaymentChannels\Drivers\Izipay;

use App\Models\Order;
use App\Models\PaymentChannel;
use App\PaymentChannels\IChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Lyra\Client as LyraClient;

class Channel implements IChannel
{
    protected $currency;
    protected $client;
    protected $publicKey;

    /**
     * Channel constructor.
     * @param PaymentChannel $paymentChannel
     */
    public function __construct(PaymentChannel $paymentChannel)
    {
        $this->currency = currency();

        $username = env('IZIPAY_CLIENT_ID');
        $password = env('IZIPAY_CLIENT_SECRET');
        $endpoint = env('IZIPAY_URL');
        $clientEndpoint = url('');
        $this->publicKey = env('IZIPAY_PUBLIC_KEY');
        $SHA256Key = base64_encode(env('IZIPAY_CLIENT_ID'));

        $client = new LyraClient();
        $client->setUsername($username);
        $client->setPassword($password);
        $client->setEndpoint($endpoint);
        $client->setClientEndpoint($clientEndpoint);
        $client->setPublicKey($this->publicKey);
        $client->setSHA256Key($SHA256Key);

        $this->client = $client;
    }

    /**
     * @throws \Exception
     */
    public function paymentRequest(Order $order)
    {
        // Send purchase request
        try {
            $authorization = base64_encode(env('IZIPAY_CLIENT_ID').':'.env('IZIPAY_CLIENT_SECRET'));

            $store = [
                "amount" => (int)($order->total_amount * 100), 
                "currency" => 'USD',//$this->currency, // worked by this currency => PEN
                "orderId" => $order->id,
                "customer" => [
                    'reference' => auth()->id(),
                    "email" => $order->user->email,
                    'billingDetails' => [
                            'firstname' => auth()->user()->full_name
                    ]
                ]
            ];

            $response =(array) Http::withHeaders([
                    'Authorization' => 'Basic '. $authorization,
                    'Accept' => 'application/json'
                ])->post(env('IZIPAY_URL'), $store)->object();
           
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }

        if ($response['status'] != 'SUCCESS') {
            throw new \Exception(trans('cart.gateway_error'));
        }
 
        $formToken = $response["answer"]->formToken;

        $data = [
            'publicKey' => $this->publicKey,
            'formToken' => $formToken,
            'successUrl' => $this->makeCallbackUrl($order, 'success'),
        ];

        return view('web.default.cart.channels.izipay', $data);
    }

    private function makeCallbackUrl($order, $status)
    {
        return url("/payments/verify/Izipay?status=$status&order_id=$order->id");
    }

    public function verify(Request $request)
    {
        try {
            if (!$this->client->checkHash()) {
                //something wrong, probably a fraud ....
                throw new \Exception('invalid signature');
            }

            $rawAnswer = $this->client->getParsedFormAnswer();
            $formAnswer = $rawAnswer['kr-answer'];
            // dd($rawAnswer);
            /* Retrieve the transaction id from the IPN data */
            $transaction = $formAnswer['transactions'][0];
            // dd($formAnswer, $rawAnswer, $transaction);
            /* get some parameters from the answer */
            $orderStatus = $formAnswer['orderStatus'];
            $orderId = $formAnswer['orderDetails']['orderId'];
            $transactionUuid = $transaction['uuid'];

            $user = auth()->user();
            
            $order = Order::where('id', $orderId)
                ->where('user_id', $user->id)
                ->first();

            if (!empty($order)) {
                
                $orderStatus = $orderStatus == 'PAID' ? Order::$paying : Order::$fail;
                $order->update([
                    'status' => $orderStatus
                ]);
            }
            return $order;

        } catch (\Exception $exception) {
            //dd($exception);
            throw new \Exception($exception->getMessage(), $exception->getCode());
        }
    }
}
