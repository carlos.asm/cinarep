<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AfilliateWebinar extends Model
{
    protected $table = 'affiliates_webinar';
    public $timestamps = false;
   
}
