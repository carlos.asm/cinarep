<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebinarNotas extends Model
{
    protected $table = 'webinar_notas';
    public $timestamps = false;
    
}
