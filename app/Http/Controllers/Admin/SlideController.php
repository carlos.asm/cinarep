<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slide;
use App\Models\Images;
use App\Models\Secciones;
use App\Models\Notifications;
use Illuminate\Support\Facades\Storage;

class SlideController extends Controller
{
    public function slides()
	{
		$query = request()->get('q');
		$sort = request()->get('sort');
		$pagination = 15;

		$data = Slide::orderBy('id','desc')->paginate($pagination);

		// Search
		if (isset($query)) {
		 	$data = Slide::where('titulo', 'LIKE', '%'.$query.'%')
			->orWhere('imagen', 'LIKE', '%'.$query.'%')
		 	->orderBy('id','desc')->paginate($pagination);
		 }

		// Sort
		if( isset( $sort ) && $sort == 'title' ) {
			$data = Slide::orderBy('id','desc')->paginate($pagination);
		}

		if( isset( $sort ) && $sort == 'active' ) {
			$data = Slide::where('status',1)->paginate($pagination);
		}



		return view('feedback.slide.index', ['data' => $data,'query' => $query, 'sort' => $sort ]);
	} //<--- End Method

    public function delete_slide(Request $request, $id)
    {

     
        $slide = Slide::find($id);

        // Delete Notification
       /* $notifications = Notifications::where('destination', $request->id)
        ->where('type', '2')
        ->orWhere('destination', $request->id)
        ->where('type', '3')
        ->orWhere('destination', $request->id)
        ->where('type', '6')
        ->get();
        
        if (isset($notifications)) {
            foreach ($notifications as $notification) {
                $notification->delete();
            }
        }*/
        
		// Borrar slide/imagen
		Storage::delete('public/uploads/'.$slide->imagen);
		
		$slide->delete();
        return redirect('admin/panel/admin/slides')->withSuccessMessage(__('Se borró exitosamente'));
		//return redirect('panel/admin/slides');

	}//<--- End Method

	public function edit_slide($id) {

		$data = Slide::findOrFail($id);
       // $secciones = Secciones::where('mode','on')->orwhere('id',$id)->orderBy('id','desc')->take(35)->get();
		return view('feedback.slide.edit', ['data' => $data]);

	}//<--- End Method

	public function update_slide(Request $request) {

		$sql = Slide::find($request->id);


		//$this->validate($request, $rules);
       
        
	  $sql->orden         = $request->orden;
      $sql->link        = $request->link;	
		$sql->titulo      = $request->titulo;
		$sql->descr = $request->descripcion;
        $sql->target = "_self";
        if(isset($request->status)&&$request->status="active")
        {$sql->status = 1;}
        else
        {$sql->status = 0;}
		$sql->save();

		return redirect('admin/panel/admin/slides')->withSuccessMessage(__('admin.success_update'));
	}//<--- End Method


    public function create_slide()
    {
        return view('feedback.slide.create');
    }


    public function store_slide(Request $request)
    {
       if(Storage::put(config('path.slide').$request->title.".jpg", $request->file('photo')->get(), 'public')) 
       {      
        $slide=new Slide;
        $slide->orden = $request->orden;
        $slide->descr = $request->descripcion;
        $slide->link = $request->link;
        $slide->imagen = $request->title.".jpg";
        $slide->titulo = $request->titulo;
        if(isset($request->status)&&$request->status="active")
        {$slide->status = 1;}
        else
        {$slide->status = 0;}        
        $slide->target = "_self";
        $slide->save();
        return redirect('panel/admin/slides')->withSuccessMessage(__('admin.success_create'));
       }
       else{
        return response()->json([
            'success' => false,
            'errors' => ['error' => trans('misc.error')],
        ]);
       }
    }

	public function set_slide($id)
    {
		
//$imagen=Images::findOrfail($id);
$folderPath =public_path('uploads/');

//$imageFullPath = $folderPath.$imagen->preview;
////file_put_contents($imageFullPath, file_get_contents(Storage::url(config('path.large').substr($imagen->preview, 0, 6)."/".$imagen->preview)));
return view('feedback.slide.cropcreate');
	}

	public function uploadCropImage(Request $request)
    {
        
		$folderPath =public_path('uploads/');
		
		//return $folderPath;
        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
 
        $imageName = $request->title.".jpg";
 
        $imageFullPath = $folderPath.$imageName;

		//Storage::disk('local')->put($imageFullPath, $image_base64);
 
        if(file_put_contents($imageFullPath, $image_base64))
       
       //if(Storage::put(config('path.slide').$imageName, $image_base64))
       {      
        $slide=new Slide;
        $slide->orden = $request->orden;
        $slide->descr = $request->descripcion;
        $slide->link = $request->link;
        $slide->imagen = $request->title.".jpg";
        $slide->titulo = $request->titulo;
        if(isset($request->status)&&$request->status="active")
        {$slide->status = 1;}
        else
        {$slide->status = 0;}        
        $slide->target = "_self";
        $slide->save();
		//unlink($imageFullPath);
        return response()->json(['success'=>'Se guardó Correctamente']);
       }
       else{
        return response()->json([
            'success' => false,
            'errors' => ['error' => 'Error'],
        ]);
       }
    
        //return response()->json(['success'=>'Crop Image Uploaded Successfully']);
    }

}
