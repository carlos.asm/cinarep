<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\WebinarAssignment;
use App\Models\WebinarNotas;
use App\Models\WebinarAssignmentAttachment;
use App\Http\Controllers\Web\traits\LearningPageAssignmentTrait;
use App\Http\Controllers\Web\traits\LearningPageForumTrait;
use App\Http\Controllers\Web\traits\LearningPageItemInfoTrait;
use App\Http\Controllers\Web\traits\LearningPageMixinsTrait;
use App\Http\Controllers\Web\traits\LearningPageNoticeboardsTrait;
use App\Models\CourseNoticeboard;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class LearningPageController extends Controller
{
    use LearningPageMixinsTrait, LearningPageAssignmentTrait, LearningPageItemInfoTrait,
        LearningPageNoticeboardsTrait, LearningPageForumTrait;

    public function index(Request $request, $slug)
    {
        
        $requestData = $request->all();

        $webinarController = new WebinarController();

        $data = $webinarController->course($slug, true);

        

        if (!$data or !$data['hasBought']) {
            abort(403);
        }

        $course = $data['course'];
        $user = $data['user'];
        $notas1=WebinarNotas::Where("webinar_id",$slug)->Where("user_id",$user->id)->first();
       

        if ($data['hasBought'] and !empty($requestData['type']) and $requestData['type'] == 'assignment' and !empty($requestData['item'])) {
            $assignmentData = $this->getAssignmentData($course, $requestData);

            $data = array_merge($data, $assignmentData);
            
        }

        if ($course->creator_id != $user->id and $course->teacher_id != $user->id and !$user->isAdmin()) {
            $unReadCourseNoticeboards = CourseNoticeboard::where('webinar_id', $course->id)
                ->whereDoesntHave('noticeboardStatus', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                })
                ->count();

            if ($unReadCourseNoticeboards) {
                $url = $course->getNoticeboardsPageUrl();
                return redirect($url);
            }
        }
        $notas=array();
        if($notas1!=null){$notas['notas']=$notas1->notas;}
        else{$notas['notas']="";}
        
        $data = array_merge($data, $notas);
        
        return view('web.default.course.learningPage.index', $data);
    }

    public function downloadAssignment($asId, $id)
    {
        $attach = WebinarAssignmentAttachment::findOrfail($id);
        $att = substr( $attach->attach, 32);  

        //https://cinare.s3.amazonaws.com/store/1022/Logo-vectorizado2.jpg

        return Storage::disk('s3')->download($att);
    }

    public function ajaxNotas(Request $request)
    {
        $slug=$request->slug;
        $notas=$request->notas;
        $registro=$notas=WebinarNotas::Where("webinar_id",$slug)->Where("user_id",Auth()->user()->id)->first();
        if($registro!=null)
        {
            $registro->notas=$request->notas;
            $registro->save();
            return "true";
        }
        else
        {
            $notas=new WebinarNotas;
            $notas->user_id=Auth()->user()->id;
            $notas->webinar_id=$slug;
            $notas->notas=$request->notas;
            $notas->save();
            return "true";
        }
        
    }

    public function ajaxAtras(Request $request)
    {
        dd($request); return "atras"; exit;
       /* $slug=$request->slug;
        $notas=$request->notas;
        $registro=$notas=WebinarNotas::Where("webinar_id",$slug)->Where("user_id",Auth()->user()->id)->first();
        if($registro!=null)
        {
            $registro->notas=$request->notas;
            $registro->save();
            return "true";
        }
        else
        {
            $notas=new WebinarNotas;
            $notas->user_id=Auth()->user()->id;
            $notas->webinar_id=$slug;
            $notas->notas=$request->notas;
            $notas->save();
            return "true";
        }*/
        
    }

    public function ajaxAdelante(Request $request)
    {
        dd($request); return "adelante"; exit;
       /* $slug=$request->slug;
        $notas=$request->notas;
        $registro=$notas=WebinarNotas::Where("webinar_id",$slug)->Where("user_id",Auth()->user()->id)->first();
        if($registro!=null)
        {
            $registro->notas=$request->notas;
            $registro->save();
            return "true";
        }
        else
        {
            $notas=new WebinarNotas;
            $notas->user_id=Auth()->user()->id;
            $notas->webinar_id=$slug;
            $notas->notas=$request->notas;
            $notas->save();
            return "true";
        }*/
        
    }
    
}
