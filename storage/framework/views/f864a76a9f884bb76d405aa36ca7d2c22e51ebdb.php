<?php
    $socials = getSocials();
    if (!empty($socials) and count($socials)) {
        $socials = collect($socials)->sortBy('order')->toArray();
    }

    $footerColumns = getFooterColumns();
?>

<footer class="footer position-relative user-select-none" style="background-color:#eeeeee; margin-top:55px;">
   

    <?php
        $columns = ['first_column','second_column','third_column','forth_column'];
    ?>

    <div class="container">
        <div class="row">
            
                <?php $__currentLoopData = $columns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $column): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-6 col-md-3">
                        <?php if(!empty($footerColumns[$column])): ?>
                            <?php if(!empty($footerColumns[$column]['title'])): ?>
                                <span class="header d-block font-weight-bold" style="font-size:1.2em;"><strong><?php echo e($footerColumns[$column]['title']); ?></strong></span>
                            <?php endif; ?>

                            <?php if(!empty($footerColumns[$column]['value'])): ?>
                                <div class="mt-20" >
                                    <?php echo $footerColumns[$column]['value']; ?>


                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            

        </div>
<hr style="border-top: 1px solid gray;">
<h4 class="text-center"> Todos los derechos reservados</h4>
<br/>
       
</footer>
<?php /**PATH C:\xampp7.4\htdocs\cinarep\resources\views/web/default/includes/footer.blade.php ENDPATH**/ ?>