<?php $__env->startPush('styles_top'); ?>
    <link rel="stylesheet" href="/assets/default/learning_page/styles.css"/>
    <link rel="stylesheet" href="/assets/default/vendors/video/video-js.min.css">
    <style>
        #app
        {
            background-color:black!important;
        }
   

        .learning-page-content
        {
            background-color:black!important;
            color:black!important;
            
        }


        .learning-page-navbar
        {
            background-color:white!important;
            background-color:black!important;
            
        }

        .panel-collapse
        {
            border-color: black!important;;  

        }
        .course-cover-container .cover-content:after {
            background-color: transparent!important;
        }
      
        .accordion-row, .tab-item
        {
            background-color:#444444!important;
            border-color: black!important;;
            color:white!important;
        }



        #collapseBtn {
        color:white;}

        @media  only screen and (min-width: 769px) {
            .botonln
        {
            height:8%; 
            margin-top:30em;
            border-color:white!important;
        }
    }

        @media  only screen and (max-width: 768px) {
      .vjs-tech { }
       .tab-pane {background-color:#444444!important;}
       .content-tab {background-color:#444444!important;}
       .nav {background-color:#444444!important;}
       .pace-done{background-color:black!important;}
    }
  
    }
        </style>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <div class="learning-page">

        <?php echo $__env->make('web.default.course.learningPage.components.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="d-flex position-relative" style="background-color:black!important;">
            <div class="learning-page-content flex-grow-1 bg-info-light p-15">
               <div class="row">
                 <div class="col-md-1">
                <button id="btnatras" class="btn btn-secondary botonln"> <</button>
                </div>             
<div class="col-md-10">
                
                <?php echo $__env->make('web.default.course.learningPage.components.content', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-md-1">
                <button id="btnadelante" class="btn btn-secondary botonln" > ></button>
            </div>  
            </div>
            </div>

            <div class="learning-page-tabs show" >
                <ul class="nav nav-tabs py-15 d-flex align-items-center justify-content-around" id="tabs-tab" role="tablist" >
                    <li class="nav-item">
                        <a class="position-relative font-14 d-flex align-items-center active" id="content-tab"
                           data-toggle="tab" href="#content" role="tab" aria-controls="content"
                           aria-selected="true">
                            <i class="learning-page-tabs-icons mr-5" >
                                <?php echo $__env->make('web.default.panel.includes.sidebar_icons.webinars', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            </i>
                            <span style="color:white!important;" class="learning-page-tabs-link-text"><?php echo e(trans('product.content')); ?></span>
                        </a>
                    </li>

                






                   

                   
                </ul>

                <div class="tab-content h-100" id="nav-tabContent">
                    <div class="pb-20 tab-pane fade show active h-100" id="content" role="tabpanel"
                         aria-labelledby="content-tab">
                        <?php echo $__env->make('web.default.course.learningPage.components.content_tab.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>

                    <div class="pb-20 tab-pane fade  h-100" id="quizzes" role="tabpanel"
                         aria-labelledby="quizzes-tab">
                        <?php echo $__env->make('web.default.course.learningPage.components.quiz_tab.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>

                    <div class="pb-20 tab-pane fade  h-100" id="certificates" role="tabpanel"
                         aria-labelledby="certificates-tab">
                        <?php echo $__env->make('web.default.course.learningPage.components.certificate_tab.index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>

              
                </div>
           
            </div>

        

        </div>
    </div>

    
 
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts_bottom'); ?>
<script src="/assets/default/js/parts/comment.min.js"></script>

    <script src="/assets/default/vendors/video/video.min.js"></script>
    <script src="/assets/default/vendors/video/youtube.min.js"></script>
    <script src="/assets/default/vendors/video/vimeo.js"></script>

    <script>
        var defaultItemType = '<?php echo e(request()->get('type')); ?>'
        var defaultItemId = '<?php echo e(request()->get('item')); ?>'
        var loadFirstContent = <?php echo e((!empty($dontAllowLoadFirstContent) and $dontAllowLoadFirstContent) ? 'false' : 'true'); ?>; // allow to load first content when request item is empty

        var courseUrl = '<?php echo e($course->getUrl()); ?>';

        // lang
        var pleaseWaitForTheContentLang = '<?php echo e(trans('update.please_wait_for_the_content_to_load')); ?>';
        var downloadTheFileLang = '<?php echo e(trans('update.download_the_file')); ?>';
        var downloadLang = '<?php echo e(trans('home.download')); ?>';
        var showHtmlFileLang = '<?php echo e(trans('update.show_html_file')); ?>';
        var showLang = '<?php echo e(trans('update.show')); ?>';
        var sessionIsLiveLang = '<?php echo e(trans('update.session_is_live')); ?>';
        var youCanJoinTheLiveNowLang = '<?php echo e(trans('update.you_can_join_the_live_now')); ?>';
        var joinTheClassLang = '<?php echo e(trans('update.join_the_class')); ?>';
        var coursePageLang = '<?php echo e(trans('update.course_page')); ?>';
        var quizPageLang = '<?php echo e(trans('update.quiz_page')); ?>';
        var sessionIsNotStartedYetLang = '<?php echo e(trans('update.session_is_not_started_yet')); ?>';
        var thisSessionWillBeStartedOnLang = '<?php echo e(trans('update.this_session_will_be_started_on')); ?>';
        var sessionIsFinishedLang = '<?php echo e(trans('update.session_is_finished')); ?>';
        var sessionIsFinishedHintLang = '<?php echo e(trans('update.this_session_is_finished_You_cant_join_it')); ?>';
        var goToTheQuizPageForMoreInformationLang = '<?php echo e(trans('update.go_to_the_quiz_page_for_more_information')); ?>';
        var downloadCertificateLang = '<?php echo e(trans('update.download_certificate')); ?>';
        var enjoySharingYourCertificateWithOthersLang = '<?php echo e(trans('update.enjoy_sharing_your_certificate_with_others')); ?>';
        var attachmentsLang = '<?php echo e(trans('public.attachments')); ?>';
        var checkAgainLang = '<?php echo e(trans('update.check_again')); ?>';
        var learningToggleLangSuccess = '<?php echo e(trans('public.course_learning_change_status_success')); ?>';
        var learningToggleLangError = '<?php echo e(trans('public.course_learning_change_status_error')); ?>';
        var sequenceContentErrorModalTitle = '<?php echo e(trans('update.sequence_content_error_modal_title')); ?>';
        var sendAssignmentSuccessLang = '<?php echo e(trans('update.send_assignment_success')); ?>';
        var saveAssignmentRateSuccessLang = '<?php echo e(trans('update.save_assignment_grade_success')); ?>';
        var saveSuccessLang = '<?php echo e(trans('webinars.success_store')); ?>';
        var changesSavedSuccessfullyLang = '<?php echo e(trans('update.changes_saved_successfully')); ?>';
        var oopsLang = '<?php echo e(trans('update.oops')); ?>';
        var somethingWentWrongLang = '<?php echo e(trans('update.something_went_wrong')); ?>';
        var notAccessToastTitleLang = '<?php echo e(trans('public.not_access_toast_lang')); ?>';
        var notAccessToastMsgLang = '<?php echo e(trans('public.not_access_toast_msg_lang')); ?>';
        var cantStartQuizToastTitleLang = '<?php echo e(trans('public.request_failed')); ?>';
        var cantStartQuizToastMsgLang = '<?php echo e(trans('quiz.cant_start_quiz')); ?>';
        var learningPageEmptyContentTitleLang = '<?php echo e(trans('update.learning_page_empty_content_title')); ?>';
        var learningPageEmptyContentHintLang = '<?php echo e(trans('update.learning_page_empty_content_hint')); ?>';
    </script>
    <script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="v5gxvm7qj1ku9la"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>

    <script src="/assets/default/js/parts/video_player_helpers.min.js"></script>
    <script src="/assets/learning_page/scripts.min.js"></script>

    <?php if((!empty($isForumPage) and $isForumPage) or (!empty($isForumAnswersPage) and $isForumAnswersPage)): ?>
        <script src="/assets/learning_page/forum.min.js"></script>
    <?php endif; ?>
    <script>
var slug = "<?php echo e($course->slug); ?>" ;
  
  $('#btnatras').click(function(){
var book_id = $(this).parent().data('id');

$.ajax
({ 
url: '/course/atras',
data: {"slug": slug, notas: $('#notas').val(),},
type: 'post',
success: function(result)
{
alert('Se guardó correctamente');
}
});
});

$('#btnadelante').click(function(){
var book_id = $(this).parent().data('id');

$.ajax
({ 
url: '/course/adelante',
data: {"slug": slug, notas: $('#notas').val(),},
type: 'post',
success: function(result)
{
alert('Se guardó correctamente');
}
});
});
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('web.default.layouts.app',['appFooter' => false, 'appHeader' => false], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp7.4\htdocs\cinarep\resources\views/web/default/course/learningPage/index.blade.php ENDPATH**/ ?>