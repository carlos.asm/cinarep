

<?php
if (empty($authUser) and auth()->check()) {
    $authUser = auth()->user();
}

$navBtnUrl = null;
$navBtnText = null;

if(request()->is('forums*')) {
    $navBtnUrl = '/forums/create-topic';
    $navBtnText = trans('update.create_new_topic');
} else {
    $navbarButton = getNavbarButton(!empty($authUser) ? $authUser->role_id : null);

    if (!empty($navbarButton)) {
        $navBtnUrl = $navbarButton->url;
        $navBtnText = $navbarButton->title;
    }
}
?>

<?php $__env->startPush('scripts_bottom'); ?>
<script src="/assets/default/js/parts/navbar.min.js"></script>
<?php $__env->stopPush(); ?>
<?php /**PATH C:\xampp7.4\htdocs\cinarep\resources\views/web/default/includes/top_nav.blade.php ENDPATH**/ ?>