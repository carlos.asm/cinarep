<?php $__env->startPush('styles_top'); ?>

<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

    <section class="container mt-45">
        

        <div class="row">
            <div class="max-w-7x1 mx-auto px-4 sm:px-6 lg:px-8 mt-10">
                <h1 class="section-title"><?php echo e(trans('financial.select_a_payment_gateway')); ?></h1>
            </div>
        </div>
        

        <form action="<?php echo e(URL::to('payments/payment-izipay')); ?>" method="post" class=" mt-25">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="order_id" value="<?php echo e($order->id); ?>">

            <div class="row">
                <?php if(!empty($paymentChannels)): ?>
                <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 mt-3">
                    <div class="space-y-8">
                        <?php $__currentLoopData = $paymentChannels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paymentChannel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="max-w-10xl charge-account-radio">
                                <input type="radio" name="gateway" id="<?php echo e($paymentChannel->title); ?>" data-class="<?php echo e($paymentChannel->class_name); ?>" value="<?php echo e($paymentChannel->id); ?>">
                                <label for="<?php echo e($paymentChannel->title); ?>" class="">
                                    <img class="h-80 w-full object-cover object-center" src="<?php echo e(url($paymentChannel->image)); ?>" alt="">

                                    <p class="mt-30 mt-lg-50 font-weight-500 text-dark-blue">
                                        <?php echo e(trans('financial.pay_via')); ?>

                                        <span class="font-weight-bold font-14"><?php echo e($paymentChannel->title); ?></span>
                                    </p>
                                </label>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                    
                <?php endif; ?>
            </div>


            <div class="d-flex align-items-center justify-content-between mt-45">
                <span class="font-16 font-weight-500 text-gray"><?php echo e(trans('financial.total_amount')); ?> <?php echo e(addCurrencyToPrice($total)); ?></span>
                <button type="button" id="paymentSubmit" disabled class="btn btn-sm btn-primary"><?php echo e(trans('public.start_payment')); ?></button>
            </div>
        </form>

        <?php if(!empty($razorpay) and $razorpay): ?>
            <form action="/payments/verify/Razorpay" method="get">
                <input type="hidden" name="order_id" value="<?php echo e($order->id); ?>">

                <script src="https://checkout.razorpay.com/v1/checkout.js"
                        data-key="<?php echo e(env('RAZORPAY_API_KEY')); ?>"
                        data-amount="<?php echo e((int)($order->total_amount * 100)); ?>"
                        data-buttontext="product_price"
                        data-description="Rozerpay"
                        data-currency="<?php echo e(currency()); ?>"
                        data-image="<?php echo e($generalSettings['logo']); ?>"
                        data-prefill.name="<?php echo e($order->user->full_name); ?>"
                        data-prefill.email="<?php echo e($order->user->email); ?>"
                        data-theme.color="#43d477">
                </script>
            </form>
        <?php endif; ?>
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts_bottom'); ?>
    <script src="/assets/default/js/parts/payment.min.js"></script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make(getTemplate().'.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp7.4\htdocs\cinarep\resources\views/web/default/cart/payment.blade.php ENDPATH**/ ?>