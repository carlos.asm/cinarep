@extends('admin.layouts.app')

@section('title'){{ trans('users.upload').' - ' }}@endsection
<meta name="_token" content="{{ csrf_token() }}">
@push('libraries_top')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<style type="text/css">
	img {
	display: block;
	max-width: 100%;
	}
	.preview {
	overflow: hidden;
	width: 160px; 
	height: 160px;
	margin: 10px;
	border: 1px solid red;
	}
	.modal-lg{
	max-width: 1000px !important;
	}
	</style>
@endpush

@section('content')

	{{--<div class="section-header">
		<h1>Nuevo Slide</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active"><a href="/admin/">{{ trans('admin/main.dashboard') }}</a>
			</div>
			<div class="breadcrumb-item active"><a href="/admin/panel/admin/slides">Slider</a>
			</div>
			<div class="breadcrumb-item">Nuevo</div>
		</div>
	</div>--}}
{{--<h5 class="mb-4 fw-light">
  <a class="text-reset" href="{{ url('panel/admin') }}">{{ __('admin.dashboard') }}</a>
    <i class="bi-chevron-right me-1 fs-6"></i>
    <a class="text-reset" href="{{ url('panel/admin/slides') }}">{{ __('misc.slide') }}</a>
    <i class="bi-chevron-right me-1 fs-6"></i>
    <span class="text-muted">{{ __('admin.create') }}</span>
    <i class="bi-chevron-right me-1 fs-6"></i>
   
</h5>--}}
<div class="content">
	

	<div class="container mt-5">
		<div class="card">
		<h2 class="card-header">Crop Imagen</h2>
		<div class="card-body">
		<h5 class="card-title">Seleccione una imagen para editar</h5>
		<input type="file" name="image" class="image">
		{{--<img src="" name="image" class="image">--}}
		</div>
		</div>  
		</div>
		{{--<img id="output" width="100%" />--}}

		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
			<form method="POST" action="{{ url('panel/admin/slide/store') }}" enctype="multipart/form-data" id="formUpload" files="true">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title" id="modalLabel">Editar Imagen y guardar slide.</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
		</button>
		</div>
		<div class="modal-body">
		<div class="img-container">
		<div class="row">
		<div class="col-md-8 ">
			{{--<img id="image" src="{{ Storage::url(config('path.preview').substr($imagens->preview, 0, 6)."/".$imagens->preview) }}">--}}
		<img id="image">
		</div>
		<div class="col-md-4">
		<div class="preview offset-md-1"></div>
		<div class="col-md-12"> 
			<div class="row mb-3 offset-md-1">
			<label class="col-sm-2 col-form-label text-lg-end">Link</label>
			<div class="col-sm-12">
				@if(isset($data))
				<input value="{{ $data->link }}" name="link" id="link" type="text" class="form-control">
				@else

				<input value="@if(isset($imagens)){{ $imagens->categories_id}}@endif" id="link" name="link" type="text" class="form-control">
				@endif
			</div>
		</div>
		<div class="row mb-3 offset-md-1">
			<label class="col-sm-2 col-form-label text-lg-end">Nombre Imagen</label>
			<div class="col-sm-12">
				<input value="@if(isset($imagens)){{ substr($imagens->preview, 0, -4) }}@endif" id="title" name="title" type="text" class="form-control">
			</div>
		</div>
	</div>
		</div>
		</div>
		</div>
		<br/><br/>
		<!-- col-md-12 -->
		<div class="col-md-12">

			<div class="card border-0">

				<div class="card-body p-0">

					<div class="row mb-3 visually-hidden">
						<input type="hidden" class="form-control tagin" id="tagInput" name="tags" placeholder="{{ trans('misc.tags') }}">

					</div>

				

					<div class="row mb-3">
						<label class="col-sm-2 col-form-label text-lg-end">Título</label>
						<div class="col-sm-10">
							
					<input value="" id="titulo" name="titulo" type="text" class="form-control">
							

						</div>
					</div>


					<input type="hidden" name="id" value="@if(isset($data)){{$data->cve}}@endif">

					<div class="row mb-3">
						<label class="col-sm-2 col-form-label text-lg-end">Orden</label>
						<div class="col-sm-10">
							
							
							<input value="" name="orden" id="orden" type="text" class="form-control">
							
						</div>
					</div>

					<div class="row mb-3">
						<label class="col-sm-2 col-form-label text-lg-end">Descripción</label>
						<div class="col-sm-10">
							
							<input value="" name="descripcion" id="desc" type="text" class="form-control @error('icon') is-invalid @enderror"">
							

						</div>
					</div>

				



					<fieldset class="row mb-3">
						<legend class="col-form-label col-sm-2 pt-0 text-lg-end">Activo?</legend>
						<div class="col-sm-10">
							<div class="form-check form-switch form-switch-md">
								<input class="form-check-input" type="checkbox" name="status" @if(isset($data)) @if ($data->status == 1) checked="checked" @endif @endif value="active" role="switch" id="status">
							</div>
						</div>
					</fieldset><!-- end row -->
	
					</form>

				</div>
			</div>

		</div>
		<!-- col-md-12-->
		</div>
		<div class="modal-footer">
		{{--<button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrar">Cancel</button>--}}
		<a href="{{ url('admin/panel/admin/slide/set_slide/1') }}" class="btn btn-secondary">
			<i class="bi bi-x-lg">Cancelar</i>
			  </a>
		<button type="button" class="btn btn-primary" id="crop">Guardar</button>

		</div>
		</div>
		</div>
	</form>
		</div>
</div><!-- content -->
</section>

@endsection

@push('scripts_bottom')



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script>	
/* $(window).on('load', function() {
        $('#modal').modal('show');
    });*/


var $modal = $('#modal');
var image = document.getElementById('image');
var cropper;
$("body").on("change", ".image", function(e){
var files = e.target.files;
var done = function (url) {
image.src = url;
$modal.modal('show');
};
var reader;
var file;
var url;
if (files && files.length > 0) {
file = files[0];
if (URL) {
done(URL.createObjectURL(file));
} else if (FileReader) {
reader = new FileReader();
reader.onload = function (e) {
done(reader.result);
};
reader.readAsDataURL(file);
}
}
});
$modal.on('shown.bs.modal', function () {
cropper = new Cropper(image, {
aspectRatio: 2,
viewMode: 1,
preview: '.preview'
});
}).on('hidden.bs.modal', function () {
cropper.destroy();
cropper = null;
});
$("#crop").click(function(){
canvas = cropper.getCroppedCanvas({
width: 2880,
height: 1620,
});
canvas.toBlob(function(blob) {
url = URL.createObjectURL(blob);
var reader = new FileReader();
reader.readAsDataURL(blob); 
reader.onloadend = function() {
var base64data = reader.result; 
$.ajax({
type: "POST",
dataType: "json",
url: "/admin/panel/admin/slide/crop-image-upload",
data: {'_token': $('meta[name="_token"]').attr('content'), 
'image': base64data, 
'orden': $('#orden').val(),
'descripcion': $('#desc').val(),
'link': $('#link').val(),
'title': $('#title').val(),
'titulo': $('#titulo').val(),
'status': $('#status').val()},
success: function(data){
	
console.log(data);
$modal.modal('hide');
alert("Se guardó correctamente");
window.location='/admin/panel/admin/slides'
}
});
}
});
})

/*var loadFile = function(event) {
	var image = document.getElementById('output');
	image.src = URL.createObjectURL(event.target.files[0]);
};*/
</script>

@endpush
