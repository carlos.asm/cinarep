

			 <input type="hidden" name="id" value="@if(isset($data)){{$data->id}}@endif">

		        <div class="row mb-3">
		          <label class="col-sm-2 col-form-label text-lg-end">Título</label>
		          <div class="col-sm-10">
		            <input value="@if(isset($data)){{ $data->titulo }}@endif" name="titulo" type="text" class="form-control">
		          </div>
		        </div>

            <div class="row mb-3">
		          <label class="col-sm-2 col-form-label text-lg-end">Orden</label>
		          <div class="col-sm-10">
		            <input value="@if(isset($data)){{ $data->orden }}@endif" name="orden" type="text" class="form-control">
		          </div>
		        </div>

            <div class="row mb-3">
		          <label class="col-sm-2 col-form-label text-lg-end">Descripcion</label>
		          <div class="col-sm-10">
		            <input value="@if(isset($data)){{ $data->descr }}@endif" name="descripcion" type="text" class="form-control">
		          </div>
		        </div>

            <div class="row mb-3">
		          <label class="col-sm-2 col-form-label text-lg-end">Link </label>
		          <div class="col-sm-10">
		            <input value="@if(isset($data)){{ $data->link }}@endif" name="link" type="text" class="form-control">
		          </div>
		        </div>

		       {{-- <div class="row mb-3">
		          <label class="col-sm-2 col-form-labe text-lg-end">{{ trans('misc.category') }}</label>
		          <div class="col-sm-10">
		            <select name="categories_id" class="form-select">
                  @foreach (Categories::where('status','1')->orderBy('seccion')->get() as $category)
                      <option @if( $data->categories_id == $category->id ) selected="selected" @endif value="{{$category->id}}">{{ $category->seccion }}</option>
                    @endforeach
		           </select>
		          </div>
		        </div>--}}

            <fieldset class="row mb-3">
              <legend class="col-form-label col-sm-2 pt-0 text-lg-end">Activo?</legend>
              <div class="col-sm-10">
                <div class="form-check form-switch form-switch-md">
                 <input class="form-check-input" type="checkbox" name="status"@if(isset($data)) @if ($data->status == 1) checked="checked" @endif @endif value="active" role="switch">
               </div>
              </div>
            </fieldset><!-- end row -->
           
            {{--<div class="row mb-3">
		          <label class="col-sm-2 col-form-labe text-lg-end">{{ trans('misc.link') }}</label>
		          <div class="col-sm-10">
		            <select name="link" class="form-select">
                 
                        
                  
                  @foreach ($secciones as $category)
                  
                     <option @if( $data->link == $category->idseccion ) selected="selected" @endif value="{{$category->idseccion}}">{{ $category->seccion }}</option>
                    @endforeach
		           </select>
		          </div>
		        </div>--]]

           {{-- <fieldset class="row mb-3">
              <legend class="col-form-label col-sm-2 pt-0 text-lg-end">{{ trans('misc.featured') }}</legend>
              <div class="col-sm-10">
                <div class="form-check form-switch form-switch-md">
                 <input class="form-check-input" type="checkbox" name="featured" @if ($data->featured == 'yes') checked="checked" @endif value="yes" role="switch">
               </div>
              </div>
            </fieldset><!-- end row -->--}}

						<div class="row mb-3">
		          <div class="col-sm-10 offset-sm-2">
		            <button type="submit" class="btn btn-dark mt-3 px-5 me-2">Guardar</button>
                {{--<a href="{{ url('slide', $data->id) }}" target="_blank" class="btn btn-link text-reset mt-3 px-3 e-none text-decoration-none">{{ __('admin.view') }} <i class="bi-box-arrow-up-right ms-1"></i></a>--}}
		          </div>
		        </div>

		      