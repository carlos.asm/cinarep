@extends('admin.layouts.app')

@section('content')
<section class="section">
  <div class="section-header">
      <h1>Slider</h1>
      <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="/admin/">Dashboard</a>
          </div>
          <div class="breadcrumb-item">Slider</div>
      </div>
  </div>

	{{--<h5 class="mb-4 fw-light">
    <a class="text-reset" href="{{ url('panel/admin') }}">{{ __('admin.dashboard') }}</a>
      <i class="bi-chevron-right me-1 fs-6"></i>
      <span class="text-muted">{{ __('misc.slide') }} ({{$data->total()}})</span>
      <a href="{{ url('panel/admin/slide/create') }}" class="btn btn-sm btn-dark float-lg-end mt-1 mt-lg-0">
				<i class="bi-plus-lg"></i> {{ trans('misc.add_new') }}
			</a>
  </h5>--}}

<div class="content">
	<div class="row">

		<div class="col-lg-12">

			@if (session('success_message'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
              <i class="bi bi-check2 me-1"></i>	{{ session('success_message') }}

                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                  <i class="bi bi-x-lg"></i>
                </button>
                </div>
              @endif

              @if (session('info_message'))
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                      <i class="bi bi-check2 me-1"></i>	{{ session('info_message') }}

                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                          <i class="bi bi-x-lg"></i>
                        </button>
                        </div>
                      @endif

			<div class="card shadow-custom border-0">
				<div class="card-body p-lg-4">

          <div class="d-lg-flex justify-content-lg-between align-items-center mb-2 w-100">
            <a href="{{ url('admin/panel/admin/slide/set_slide/1') }}" class="btn btn-sm btn-dark float-lg-end mt-1 mt-lg-0">
              <i class="bi-plus-lg"></i> Agregar
            </a>
          </div>

        <div class="table-responsive p-0">
            <table class="table table-hover">
         <tbody>

          @if ($data->total() !=  0 && $data->count() != 0)
             <tr>
                <th class="active">ID</th>
                <th class="active">Imágen</th>
                <th class="active">Título</th>
                <th class="active">Nombre</th>
                <th class="active"> Orden </th>
               
                <th class="active">Link</th>
                
                <th class="active">Status</th>
                <th class="active">Acciones</th>
              </tr>

            @foreach ($data as $image)
              <tr>
                <td>{{ $image->id }}</td>
              
                <td><img src="{{'/uploads/'.$image->imagen}}" class="rounded" width="200" /></td>
                <td><a href="{{ url('panel/admin/slide', $image->id) }}" title="{{$image->titulo}}" target="_blank">{{ \Illuminate\Support\Str::limit($image->titulo, 10, '...') }} <i class="fa fa-external-link-square"></i></a></td>
                <td>{{ $image->imagen }}</td>
                <td>{{ $image->orden }}</td>
                <td>{{ $image->link }}</td>
                
                
                <td>
                  <span class="badge rounded-pill bg-{{ $image->status == 1 ? 'success' : 'warning' }}">
                    {{ $image->status == 1 ? 'Active' : 'Inactive' }}
                </span>
              </td>
                <td>

             <a href="{{ url('admin/panel/admin/slide', $image->id) }}" class="text-reset fs-5 me-2">
                    <i class="far fa-edit"></i>
                  </a>
               
   

      @include('admin.includes.delete_button',['url' => '/admin/panel/admin/slide/delete/'.$image->id])

          </td>
        </tr><!-- /.TR -->
        @endforeach

              @else
                <h5 class="text-center p-5 text-muted fw-light m-0">
                  {{ trans('misc.no_results_found') }}

                  @if (isset($query) || isset($sort))
                    <div class="d-block w-100 mt-2">
                      <a href="{{url('panel/admin/slide')}}"><i class="bi-arrow-left me-1"></i> {{ trans('auth.back') }}</a>
                    </div>
                  @endif
                </h5>

              @endif

            </tbody>
            </table>
          </div><!-- /.table responsive -->
				 </div><!-- card-body -->
 			</div><!-- card  -->

      {{ $data->appends(['q' => $query, 'sort' => $sort])->onEachSide(0)->links() }}
 		</div><!-- col-lg-12 -->

	</div><!-- end row -->
</div><!-- end content -->

</div> <!-- Section -->
@endsection
