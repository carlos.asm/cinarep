@extends('admin.layout')

@section('title'){{ trans('users.upload').' - ' }}@endsection

@section('css')
<link href="{{ asset('public/js/tagin/tagin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<h5 class="mb-4 fw-light">
	<a class="text-reset" href="{{ url('panel/admin') }}">{{ __('admin.dashboard') }}</a>
	<i class="bi-chevron-right me-1 fs-6"></i>
	<a class="text-reset" href="{{ url('panel/admin/slides') }}">{{ __('misc.slide') }}</a>
	<i class="bi-chevron-right me-1 fs-6"></i>
	<span class="text-muted">{{ __('admin.create') }}</span>
	<i class="bi-chevron-right me-1 fs-6"></i>

</h5>
<div class="content">

	<div class="container pt-5">
		<div class="row">

			@if (auth()->user()->status == 'active')

			@if ($settings->limit_upload_user == 0
			|| auth()->user()->dailyUploads() < $settings->limit_upload_user
				|| auth()->user()->isSuperAdmin()
				)

				<div class="col-md-12 mb-12">
					<!-- form start -->
					<form method="POST" action="{{ url('panel/admin/slide/store') }}" enctype="multipart/form-data" id="formUpload" files="true">

						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<!-- wrapper upload -->
						<div class="filer-input-dragDrop position-relative" id="draggable" style="height:300px;">

							<input type="file" accept="image/*" name="photo" id="filePhoto">

							<!-- previewPhoto -->
							<div class="previewPhoto"></div><!-- previewPhoto -->

							<span class="text-dark btn-remove-photo display-none c-pointer" id="removePhoto">
								<i class="bi bi-x-lg text-white"></i>
							</span>

							<div class="filer-input-inner">
								<div class="filer-input-icon">
									<i class="bi bi-image"></i>
								</div>
								<div class="filer-input-text">
									<h3 class="mb-2 fw-light">{{ trans('misc.click_select_image') }}</h3>
									<h3 class="fw-light">{{ trans('misc.max_size') }}: {{ $settings->min_width_height_image.' - '.Helper::formatBytes($settings->file_size_allowed * 1024)}} </h3>
								</div>
							</div>
						</div><!-- ./ wrapper upload -->



				</div>

				<!-- col-md-12 -->
				<div class="col-md-12">

					<div class="card border-0">

						<div class="card-body p-0">

							<div class="row mb-3 visually-hidden">
								<input type="hidden" class="form-control tagin" id="tagInput" name="tags" placeholder="{{ trans('misc.tags') }}">

							</div>

							<div class="row mb-3">
								<label class="col-sm-2 col-form-label text-lg-end">{{ trans('admin.image') }}</label>
								<div class="col-sm-10">
									<input value="@if(isset($data)){{ $data->titulo }}@endif" id="title" name="title" type="text" class="form-control">
								</div>
							</div>

							<div class="row mb-3">
								<label class="col-sm-2 col-form-label text-lg-end">{{ trans('admin.title') }}</label>
								<div class="col-sm-10">
									<input value="@if(isset($data)){{ $data->titulo }}@endif" id="title" name="titulo" type="text" class="form-control">
								</div>
							</div>



							<input type="hidden" name="id" value="@if(isset($data)){{$data->id}}@endif">



							<div class="row mb-3">
								<label class="col-sm-2 col-form-label text-lg-end">{{ trans('admin.orden') }}</label>
								<div class="col-sm-10">
									<input value="@if(isset($data)){{ $data->orden }}@endif" name="orden" type="text" class="form-control">
								</div>
							</div>

							<div class="row mb-3">
								<label class="col-sm-2 col-form-label text-lg-end">{{ trans('admin.descripcion') }}</label>
								<div class="col-sm-10">
									<input value="@if(isset($data)){{ $data->descr }}@endif" name="descripcion" type="text" class="form-control">
								</div>
							</div>

							<div class="row mb-3">
								<label class="col-sm-2 col-form-label text-lg-end">{{ trans('admin.link') }}</label>
								<div class="col-sm-10">
									@if(isset($data))
									<input value="{{ $data->link }}" name="link" type="text" class="form-control">
									@else

									<input value="" name="link" type="text" class="form-control">
									@endif
								</div>
							</div>



							<fieldset class="row mb-3">
								<legend class="col-form-label col-sm-2 pt-0 text-lg-end">{{ trans('admin.status') }}</legend>
								<div class="col-sm-10">
									<div class="form-check form-switch form-switch-md">
										<input class="form-check-input" type="checkbox" name="status" @if(isset($data)) @if ($data->status == 1) checked="checked" @endif @endif value="active" role="switch">
									</div>
								</div>
							</fieldset><!-- end row -->


							<div class="row mb-3">
								<div class="col-sm-10 offset-sm-2">
									<button type="submit" class="btn btn-dark mt-3 px-5 me-2 btn-custom w-100">{{ __('admin.save') }}</button>
									{{--<a href="{{ url('slide', $data->id) }}" target="_blank" class="btn btn-link text-reset mt-3 px-3 e-none text-decoration-none">{{ __('admin.view') }} <i class="bi-box-arrow-up-right ms-1"></i></a>--}}
								</div>
							</div>





							{{--}}
							<div class="box-footer text-center">
								<button type="submit" id="upload" class="btn btn-lg btn-custom w-100" data-msg-processing="{{trans('misc.processing')}}" data-error="{{trans('misc.error')}}" data-msg-error="{{trans('misc.err_internet_disconnected')}}">
									<i class="bi bi-cloud-arrow-up-fill me-1"></i> {{ trans('users.upload') }}
								</button>
							</div><!-- /.box-footer -->--}}
							</form>

						</div>
					</div>

				</div>
				<!-- col-md-12-->

				@else
				<h3 class="mt-0 text-center fw-light">
					<span class="w-100 d-block mb-4 display-1 text-warning">
						<i class="bi bi-exclamation-triangle-fill"></i>
					</span>

					{{trans('misc.limit_uploads_user')}}
				</h3>
				@endif

				@else
				<h3 class="mt-0 text-center fw-light">
					<span class="w-100 d-block mb-4 display-1 text-warning">
						<i class="bi bi-exclamation-triangle-fill"></i>
					</span>

					{{trans('misc.confirm_email')}} <span class="fw-bold">{{auth()->user()->email}}</span>
				</h3>
				@endif
				{{-- Verify User Active --}}

		</div><!-- row -->
	</div><!-- container -->
	</section>
	@endsection

	@section('javascript')
	<script src="{{ asset('public/js/tagin/tagin.min.js') }}" type="text/javascript"></script>


	<script type="text/javascript">
		const tagin = new Tagin(document.querySelector('.tagin'), {
			enter: true,
			placeholder: '{{ trans("misc.add_tag") }}',
		});

		$(".tagin").on('change', function() {
			var input = $(this).siblings('.tagin-wrapper');
			var maxLen = 
				
					{{$settings->tags_limit}}
				
			;

			if (input.children('span.tagin-tag').length >= maxLen) {
				input.children('input.tagin-input').addClass('d-none');
			} else {
				input.children('input.tagin-input').removeClass('d-none');
			}
		});

		function replaceString(string) {
			return string.replace(/[\-\_\.\+]/ig, ' ')
		}

		$('#removePhoto').click(function() {
			$('#filePhoto').val('');
			$('#title').val('');
			$('.previewPhoto').css({
				backgroundImage: 'none'
			}).hide();
			$('.filer-input-dragDrop').removeClass('hoverClass');
			$(this).hide();
		});

		//================== START FILE IMAGE FILE READER
		$("#filePhoto").on('change', function() {

			var loaded = false;
			if (window.File && window.FileReader && window.FileList && window.Blob) {
				if ($(this).val()) { //check empty input filed
					oFReader = new FileReader(), rFilter = /^(?:image\/jpeg|image\/jpeg|image\/jpeg|image)$/i;
					if ($(this)[0].files.length === 0) {
						return
					}


					var oFile = $(this)[0].files[0];
					var fsize = $(this)[0].files[0].size; //get file size
					var ftype = $(this)[0].files[0].type; // get file type


					if (!rFilter.test(oFile.type)) {
						$('#filePhoto').val('');
						$('.popout').addClass('popout-error').html("{{ trans('misc.formats_available') }}").fadeIn(500).delay(5000).fadeOut();
						return false;
					}

					var allowed_file_size = 
						
							{{$settings->file_size_allowed * 1024}}
					;

					if (fsize > allowed_file_size) {
						$('#filePhoto').val('');
						$('.popout').addClass('popout-error').html("{{trans('misc.max_size').': '.Helper::formatBytes($settings->file_size_allowed * 1024)}}").fadeIn(500).delay(5000).fadeOut();
						return false;
					}
					<?php $dimensions = explode('x', "999x299"); ?>

					oFReader.onload = function(e) {

						var image = new Image();
						image.src = oFReader.result;

						image.onload = function() {

							if (image.width < {{
										$dimensions[0]
									}}) {
								$('#filePhoto').val('');
								$('.popout').addClass('popout-error').html("{{trans('misc.width_min',['data' => $dimensions[0]])}}").fadeIn(500).delay(5000).fadeOut();
								return false;
							}

							if (image.height < {{
										$dimensions[1]
									}}) {
								$('#filePhoto').val('');
								$('.popout').addClass('popout-error').html("{{trans('misc.height_min',['data' => $dimensions[1]])}}").fadeIn(500).delay(5000).fadeOut();
								return false;
							}

							$('.previewPhoto').css({
								backgroundImage: 'url(' + e.target.result + ')'
							}).show();
							$('#removePhoto').show();
							$('.filer-input-dragDrop').addClass('hoverClass');
							var _filname = oFile.name;
							var fileName = _filname.substr(0, _filname.lastIndexOf('.'));
							$('#title').val(replaceString(fileName));
						}; // <<--- image.onload


					}

					oFReader.readAsDataURL($(this)[0].files[0]);

				}
			} else {
				$('.popout').html('Can\'t upload! Your browser does not support File API! Try again with modern browsers like Chrome or Firefox.').fadeIn(500).delay(5000).fadeOut();
				return false;
			}
		});

		$('input[type="file"]').attr('title', window.URL ? ' ' : '');



		$('#typeImage').on('change', function() {
			if ($(this).val() == 'vector') {
				$('#vector').slideDown();
			} else {
				$('#vector').slideUp('fast');
				$('#uploadFile').val('');
				$('#fileDocument').html('');
			}
		});



		$(document).on('click', '#deleteFile', function() {
			$('#uploadFile').val('');
			$('#fileDocument').html('');
		});

		//================== START FILE - FILE READER
		$("#uploadFile").change(function() {

			$('#fileDocument').html('');

			var loaded = false;
			if (window.File && window.FileReader && window.FileList && window.Blob) {
				if ($(this).val()) { //check empty input filed
					if ($(this)[0].files.length === 0) {
						return
					}

					var oFile = $(this)[0].files[0];
					var fsize = $(this)[0].files[0].size; //get file size
					var ftype = $(this)[0].files[0].type; // get file type

					var allowed_file_size = {{$settings->file_size_allowed_vector * 1024	}};

					if (fsize > allowed_file_size) {
						$('.popout').addClass('popout-error').html("{{trans('misc.max_size_vector').': '.Helper::formatBytes($settings->file_size_allowed_vector * 1024)}}").fadeIn(500).delay(4000).fadeOut();
						$(this).val('');
						return false;
					}

					$('#fileDocument').html('<i class="fa fa-paperclip"></i> <strong class="text-muted"><em>' 
						+ oFile.name + '</em></strong> - <a href="javascript:void(0);" id="deleteFile" class="text-danger"> Delete </a>');

				}
			} else {
				alert('Can\'t upload! Your browser does not support File API! Try again with modern browsers like Chrome or Firefox.');
				return false;
			}
		});
		//================== END FILE - FILE READER ==============>
	</script>


	@endsection