@extends('admin.layouts.app')

@section('content')
<section class="section">
  <div class="section-header">
      <h1>Editar Slide</h1>
      <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="/admin/">{{ trans('admin/main.dashboard') }}</a>
          </div>
          <div class="breadcrumb-item active"><a href="/admin/panel/admin/slides">Slider</a>
          </div>
          <div class="breadcrumb-item">Editar</div>
      </div>
  </div>

	{{--<h5 class="mb-4 fw-light">
    <a class="text-reset" href="{{ url('panel/admin') }}">{{ __('admin.dashboard') }}</a>
      <i class="bi-chevron-right me-1 fs-6"></i>
      <a class="text-reset" href="{{ url('panel/admin/slides') }}">{{ __('misc.slide') }}</a>
      <i class="bi-chevron-right me-1 fs-6"></i>
      <span class="text-muted">{{ __('admin.edit') }}</span>
      <i class="bi-chevron-right me-1 fs-6"></i>
      <span class="text-muted">{{ $data->title }}</span>
  </h5>--}}

<div class="content">
	<div class="row">

		<div class="col-lg-12">

    

			<div class="card shadow-custom border-0">
				<div class="card-body p-lg-5">

          <div class="row mb-3">           
              
            <img src="{{'/public/uploads/'.$data->imagen}}" style="max-width: 800px;">
          
        </div>
        <form class="form-horizontal" method="POST" action="{{ url('admin/panel/admin/slide/update') }}" enctype="multipart/form-data">
          @csrf         
				@include('feedback.slide._form')
      </form>
				 </div><!-- card-body -->
 			</div><!-- card  -->
 		</div><!-- col-lg-12 -->

	</div><!-- end row -->
</div><!-- end content -->
</div>
@endsection
