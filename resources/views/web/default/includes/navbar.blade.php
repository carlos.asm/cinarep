@php
    $userLanguages = !empty($generalSettings['site_language']) ? [$generalSettings['site_language'] => getLanguages($generalSettings['site_language'])] : [];

    if (!empty($generalSettings['user_languages']) and is_array($generalSettings['user_languages'])) {
        $userLanguages = getLanguages($generalSettings['user_languages']);
    }

    $localLanguage = [];

    foreach($userLanguages as $key => $userLanguage) {
        $localLanguage[localeToCountryCode($key)] = $userLanguage;
    }

@endphp

<div class="top-navbar d-flex border-bottom">
    <div class="container d-flex justify-content-between flex-column flex-lg-row">
        <div class="top-contact-box border-bottom d-flex flex-column flex-md-row align-items-center justify-content-center">


            <a class=" navbar-brand navbar-order d-flex align-items-center justify-content-center mr-0 {{ (empty($navBtnUrl) and empty($navBtnText)) ? 'ml-auto' : '' }}" href="/">
                @if(!empty($generalSettings['logo']))
                    
                @endif
                <img src="{{asset('store/1/default_images/website-logo.png')}}" class="img-cover" alt="site logo">
            </a>

            

            <ul class="navbar-nav mr-auto d-flex align-items-center">
            @if(!empty($categories) and count($categories))
            <li class="mr-lg-25">
                <div class="menu-category">
                    <ul>
                        <li class="cursor-pointer user-select-none d-flex xs-categories-toggle">
                            <i data-feather="grid" width="20" height="20" class="mr-10 d-none d-lg-block"></i>
                            {{ trans('categories.categories') }}

                            <ul class="cat-dropdown-menu">
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{ (!empty($category->subCategories) and count($category->subCategories)) ? '#!' : asset($category->getUrl()) }}">
                                            <div class="d-flex align-items-center">
                                                <img src="{{ $category->icon }}" class="cat-dropdown-menu-icon mr-10" alt="{{ $category->title }} icon">
                                                {{ $category->title }}
                                            </div>

                                            @if(!empty($category->subCategories) and count($category->subCategories))
                                                <i data-feather="chevron-right" width="20" height="20" class="d-none d-lg-inline-block ml-10"></i>
                                                <i data-feather="chevron-down" width="20" height="20" class="d-inline-block d-lg-none"></i>
                                            @endif
                                        </a>

                                        @if(!empty($category->subCategories) and count($category->subCategories))
                                            <ul class="sub-menu">
                                                @foreach($category->subCategories as $subCategory)
                                                    <li>
                                                        <a href="{{ $subCategory->getUrl() }}">
                                                            @if(!empty($subCategory->icon))
                                                            <img src="{{ $subCategory->icon }}" class="cat-dropdown-menu-icon mr-10" alt="{{ $subCategory->title }} icon">
                                                            @endif

                                                            {{ $subCategory->title }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </li>
        @endif
        </div>
            <div class="d-flex align-items-center justify-content-between justify-content-md-center buscar">
                     {{-- @if(!empty($navbarPages) and count($navbarPages))
                   
                        @foreach($navbarPages as $navbarPage)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ $navbarPage['link'] }}">{{ $navbarPage['title'] }}</a>
                            </li>
                        @endforeach
                    @endif--}}
               {{-- @if(!empty($localLanguage) and count($localLanguage) > 1)
                    <form action="/locale" method="post" class="mr-15 mx-md-20">
                        {{ csrf_field() }}

                        <input type="hidden" name="locale">

                        <div class="language-select">
                            <div id="localItems"
                                 data-selected-country="{{ localeToCountryCode(mb_strtoupper(app()->getLocale())) }}"
                                 data-countries='{{ json_encode($localLanguage) }}'
                            ></div>
                        </div>
                    </form>
                @else--}}
                    {{--<div class="mr-15 mx-md-20"></div>--}
               {{-- @endif--}}

                
                <form action="/search" method="get" class="form-inline my-2 my-lg-0 navbar-search position-relative">
                    <input style="background-color:#bfdfff!important; width:30em;" class="form-control mr-5 rounded" type="text" name="search" placeholder="{{ trans('navbar.search_anything') }}" aria-label="Search">

                    <button style="border-left-style: none; margin-right:-5px; height:100%; border-radius: 5px; background-color:#296dfe!important; color:white;" 
                    type="submit" class="btn-transparent d-flex align-items-center justify-content-center search-icon">
                        <i style="padding-left:0.2em;" data-feather="search" width="20" height="20" class="mr-10"></i>
                    </button>
                </form>
            </div>
        

        <div class="xs-w-100 d-flex align-items-center justify-content-between ">
            <div class="d-flex align-items-center justify-content-center logo2">
                {{--@if(!empty($generalSettings['site_phone']))
                    <span class="d-flex align-items-center py-10 py-lg-0 text-dark-blue font-14">
                        <i data-feather="phone" width="20" height="20" class="mr-10"></i>
                        {{ $generalSettings['site_phone'] }}
                    </span>
                @endif--}}
                <div class="dropdown">
                    <button class="btn  dropdown-toggle btnlogo" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i style="color:#296dfe!important;" class="fa-solid fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @if(!empty($categories) and count($categories))
                        @foreach($categories as $category)
                        <a href="{{ (!empty($category->subCategories) and count($category->subCategories)) ? '#!' : $category->getUrl() }}">
                            <div class="d-flex align-items-center">
                                <img src="{{ $category->icon }}" class="cat-dropdown-menu-icon mr-10" alt="{{ $category->title }} icon">
                                {{ $category->title }}
                            </div>

                            @if(!empty($category->subCategories) and count($category->subCategories))
                                <i data-feather="chevron-right" width="20" height="20" class="d-none d-lg-inline-block ml-10"></i>
                                <i data-feather="chevron-down" width="20" height="20" class="d-inline-block d-lg-none"></i>
                            @endif
                        </a>                    
                      @endforeach
                      @endif
                    </div>
                  </div>
                  <a href="/">
                <img src="/store/1/default_images/logo2.png" class="img-cover" alt="site logo">
            </a>
            </div>

            <div class="d-flex">
                {{--@include(getTemplate().'.includes.catmob')
                <div class="border-left mx-5 mx-lg-15"></div>--}}
                @include(getTemplate().'.includes.shopping-cart-dropdwon')

                <div class="border-left mx-5 mx-lg-15"></div>

                @include(getTemplate().'.includes.notification-dropdown')
            </div>

            @if(!empty($authUser))


                <div class="dropdown">
                    <a href="#!" class="navbar-user d-flex align-items-center ml-50 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ $authUser->getAvatar() }}" class="rounded-circle" alt="{{ $authUser->full_name }}">
                        <span class="font-16 user-name ml-10 text-dark-blue font-14">{{ $authUser->full_name }}</span>
                    </a>

                    <div class="dropdown-menu user-profile-dropdown" aria-labelledby="dropdownMenuButton">
                        <div class="d-md-none border-bottom mb-20 pb-10 text-right">
                            <i class="close-dropdown" data-feather="x" width="32" height="32" class="mr-10"></i>
                        </div>

                        <a class="dropdown-item" href="{{ $authUser->isAdmin() ? '/admin' : '/panel' }}">
                            <img src="/assets/default/img/icons/sidebar/dashboard.svg" width="25" alt="nav-icon">
                            <span class="font-14 text-dark-blue">{{ trans('public.my_panel') }}</span>
                        </a>
                        @if($authUser->isTeacher() or $authUser->isOrganization())
                            <a class="dropdown-item" href="{{ $authUser->getProfileUrl() }}">
                                <img src="/assets/default/img/icons/profile.svg" width="25" alt="nav-icon">
                                <span class="font-14 text-dark-blue">{{ trans('public.my_profile') }}</span>
                            </a>
                        @endif
                        <a class="dropdown-item" href="/logout">
                            <img src="/assets/default/img/icons/sidebar/logout.svg" width="25" alt="nav-icon">
                            <span class="font-14 text-dark-blue">{{ trans('panel.log_out') }}</span>
                        </a>
                    </div>
                </div>
            @else
                <div class="d-flex align-items-center ml-md-50">
                    <a style="border: 1px solid #444444; border-radius: 5px;" href="/login" class="py-5 px-10 mr-10 text-dark-blue font-14">{{-- trans('auth.login') --}}Login</a>
                    <a style="border-radius: 5px;" href="/register" class="btn-primary py-5 px-10 text-dark-blue font-14 btnreg">{{ trans('auth.register') }}</a>
                </div>
            @endif
        </div>
    </div>
</div>


@push('scripts_bottom')
    <link href="/assets/default/vendors/flagstrap/css/flags.css" rel="stylesheet">
    <script src="/assets/default/vendors/flagstrap/js/jquery.flagstrap.min.js"></script>
    <script src="/assets/default/js/parts/top_nav_flags.min.js"></script>
@endpush






