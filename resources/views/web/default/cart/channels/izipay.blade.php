@extends(getTemplate().'.layouts.app')
@push('head')
<script 
        src="https://static.micuentaweb.pe/static/js/krypton-client/V4.0/stable/kr-payment-form.min.js"
        kr-public-key="{{ $publicKey }}" 
        kr-post-url-success="{{$successUrl}}">
    </script>
    <link rel="stylesheet" 
        href="https://static.micuentaweb.pe/static/js/krypton-client/V4.0/ext/classic-reset.css">
     <script
        src="https://static.micuentaweb.pe/static/js/krypton-client/V4.0/ext/classic.js">
     </script> 
@endpush
@push('styles_top')
@endpush

@section('content')
<section style="position: relative;">
    <div class="kr-embedded"  kr-form-token="{{ $formToken }}" style="margin: 0 auto;
  top: 50%;
  transform: translate(0, 10%);">

        <!-- payment form fields -->
        <div class="kr-pan"></div>
        <div class="kr-expiry"></div>
        <div class="kr-security-code"></div>

        <!-- payment form submit button -->
        <button class="kr-payment-button"></button>
<!-- error zone -->
        <div class="kr-form-error"></div>
    </div>
</section>
@endsection
@push('scripts_bottom')
    <script src="/assets/default/js/parts/payment.min.js"></script>
@endpush
