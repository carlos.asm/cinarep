<h2 class="mb-4" id="">Política de Cookies</h2>



<p>Este sitio web utiliza cookies y otros mecanismos similares. Una cookie es un pequeño fichero de texto que se almacena en 
    su navegador cuando visita casi cualquier página web. Su utilidad es que la web sea capaz de recordar su visita cuando vuelva a navegar por esa página.
     Las cookies suelen almacenar información de carácter técnico, preferencias personales, personalización de contenidos, estadísticas de uso, enlaces a 
     redes sociales, acceso a cuentas de usuario, etc. El objetivo de la cookie es adaptar el contenido de la web a su perfil y necesidades, sin cookies los 
     servicios ofrecidos por cualquier página se verían mermados notablemente.
La utilización del presente sitio web por su parte, implica que presta su consentimiento expreso e inequívoco a la utilización de cookies, en los términos 
y condiciones previstos en esta Política de Cookies, sin perjuicio de las medidas de desactivación y eliminación de las cookies que pueda adoptar.<p>

    <h3>1. Cookies utilizadas en este sitio web</h3>
    <p>Siguiendo las directrices de la Protección de Datos procedemos a detallar el uso de cookies que hace esta web con el fin de informarle con la máxima exactitud posible.
Este sitio web utiliza cookies propias para personalizar y facilitar al máximo la navegación del usuario. Las cookies se asocian únicamente a un usuario anónimo y su ordenador y no proporcionan referencias que permitan deducir datos personales del usuario. El usuario podrá configurar su navegador para que notifique y rechace la instalación de las cookies enviadas por este sitio web, sin que ello perjudique la posibilidad del usuario de acceder a los contenidos. Sin embargo, le hacemos notar que, en todo caso, la calidad de funcionamiento del sitio web puede disminuir.
    </p>
    <h3>2. Este sitio web utiliza las siguientes cookies de terceros:</h3>
    <p>Google Analytics: Almacena cookies para poder elaborar estadísticas sobre el tráfico y volumen de visitas de esta web. Al utilizar este sitio web está consintiendo el tratamiento de información acerca de usted por Google. Por tanto, el ejercicio de cualquier derecho en este sentido deberá hacerlo comunicando directamente con Google.
Redes sociales: Cada red social utiliza sus propias cookies para que usted pueda pinchar en botones del tipo Me gusta o Compartir.
    </p>
    
    <h3>3. Desactivación o eliminación de cookies</h3>
    <p>En cualquier momento podrá ejercer su derecho de desactivación o eliminación de cookies de este sitio web. 
Estas acciones se realizan de forma diferente en función del navegador que esté usando.
Todos los navegadores modernos permiten cambiar la configuración de cookies. Estos ajustes normalmente se
 encuentran en las ‘opciones’ o ‘Preferencias’ del menú de su navegador. Asimismo, puede configurar su navegador
  o su gestor de correo electrónico, así como instalar complementos gratuitos para evitar que se descarguen los Web Bugs al abrir un email.
A continuación, se ofrece orientación al usuario sobre los pasos para acceder al menú de configuración de las cookies y, en su caso, de la 
navegación privada en cada uno de los navegadores principales:
Internet Explorer: Herramientas -> Opciones de Internet -> Privacidad -> Configuración.
Para más información, puede consultar el soporte de Microsoft o la Ayuda del navegador.
Firefox: Herramientas -> Opciones -> Privacidad -> Historial -> Configuración Personalizada.
Para más información, puede consultar el soporte de Mozilla o la Ayuda del navegador.
Chrome: Configuración -> Mostrar opciones avanzadas -> Privacidad -> Configuración de contenido.
Para más información, puede consultar el soporte de Google o la Ayuda del navegador.
Safari: Preferencias -> Seguridad.
Para más información, puede consultar el soporte de Apple o la Ayuda del navegador.

<h3>¿Qué ocurre si desactivo las cookies?</h3>
<p>Para que entienda el alcance que puede tener desactivar las cookies le mostramos unos ejemplos:
El sitio web no podrá adaptar los contenidos a sus preferencias personales.
No será posible personalizar sus preferencias geográficas como, por ejemplo, el idioma.
No podrá compartir contenidos de esa web en Facebook, Twitter o cualquier otra red social. Todas las redes sociales usan cookies, si las desactiva no podrá utilizar ninguna red social.
El sitio web no podrá realizar analíticas web sobre visitantes y tráfico en la web, lo que dificultará que la web sea competitiva.
    </p>
    <h3>4. Notas adicionales</h3>
    <p>Ni este sitio web ni sus representantes legales se hacen responsables ni del contenido ni de la veracidad de las políticas de privacidad que puedan tener los terceros mencionados en esta política de cookies.
Los navegadores web son las herramientas encargadas de almacenar las cookies y desde este lugar debe efectuar su derecho a eliminación o desactivación de las mismas. Ni este sitio web ni sus representantes legales pueden garantizar la correcta o incorrecta manipulación de las cookies por parte de los mencionados navegadores.
En algunos casos es necesario instalar cookies para que el navegador no olvide su decisión de no aceptación de las mismas.
En el caso de las cookies de Google Analytics, esta empresa almacena las cookies en servidores ubicados en Estados Unidos y se compromete a no compartirla con terceros, excepto en los casos en los que sea necesario para el funcionamiento del sistema o cuando la ley obligue a tal efecto. Según Google no guarda su dirección IP. Puede consultar información detallada a este respecto en este enlace. Si desea información sobre el uso que Google da a las cookies le adjuntamos este otro enlace.
Para cualquier duda o consulta acerca de esta política de cookies no dude en comunicarse con nosotros mediante la dirección de correo hola@cinare.org
    </p>