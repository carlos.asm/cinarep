<h2>Preguntas Frecuentes</h2>

<br/><br/>
<div id="accordion">
    <div class="card">
      <div class="card-header" id="headingOne">
        <h5 class="mb-0">
          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <h3>Me da miedo hacer pagos online</h3>
          </button>
        </h5>
      </div>
  
      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
        <p>  
            No te preocupes el pago es realizado de forma 100% segura por medio de Niubiz y Paypal, El pago se lo haces directamente a ellos y son ellos nos confirman el pago y inmediato tiene acceso para ver el curso en la misma plataforma de Cinare, así que no hay riesgo alguno. Inmediatamente haces el pago te llega correos de bienvenida, registro y confirmación de pago.
        </p> 
        </div>

      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            
        <h3> No sé si online se aprenda perfectamente</h3>
          </button>
        </h5>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body">

        <p> Claro que sí, todas las técnicas son muy sencillas de realizar y el curso está diseñado para que puedas aprender desde casa perfectamente por medio de vídeos paso a paso. Además, cuentas con apoyo y soporte del docente a cargo a través de la comunicación por medio de mensajes en la misma plataforma. 
        </p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingThree">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        <h3> No tengo tiempo de ver el curso</h3>
          </button>
        </h5>
      </div>
      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
        <div class="card-body">
          
        <p> No te preocupes, el curso no tiene un horario fijo, tu podrás ingresar a la plataforma en el horario que tú quieras e ir viendo los videos a tu propio ritmo, tendrás acceso de por vida a la plataforma.
        </p>
        </div>
      </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingFour">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          <h3> ¿Cómo me llega el curso?</h3>
            </button>
          </h5>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
          <div class="card-body">
            
          <p> 
            Inmediatamente después de hacer el pago te llega al correo mensajes de bienvenida, registro y confirmación de pago, para acceder a la plataforma. Disfruta tu proceso de aprendizaje y aprovecha al máximo.
            </p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingFive">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          <h3> ¿Qué métodos de pago hay?</h3>
            </button>
          </h5>
        </div>
        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
          <div class="card-body">
            
          <p> 
Puedes pagar a través de la pasarela de pago PayU y Paypal con cualquier tipo de tarjeta de Débito/Crédito o solicitar un ticket de pago en efectivo para cancelarlo en los locales de conveniencia de tu país (Pago - Efectivo, Oxxo, Baloto, Sencillito, etc)  (En caso de que realices un pago en efectivo, recuerda que tienes hasta 3 días para poder pagar el ticket de pago en efectivo, caso contrario el código que se generara ya no será válido)
Comunícate con un asesor vía WhatsApp o escríbenos un correo al hola@cinare.org
</p>
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header" id="headingSix">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          <h3>  ¿Qué ventajas hay de comprar un curso online?</h3>
            </button>
          </h5>
        </div>
        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
          <div class="card-body">
            
          <p> 
           
            Un curso online no supone grandes gastos de logística, y garantiza una buena calidad de aprendizaje.
            Puedes llevar el curso acorde a tu disposición de tiempo, acceso las 24 horas del día.
            
</p>
          </div>
        </div>
      </div>
  </div>