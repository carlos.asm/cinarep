<h2>Programa de afiliados</h2>

<br/><br/>

<p>En Cinare el principal objetivo es compartir el conocimiento a través de los cursos online y producción académica de artículos científicos y/o libros, para de este modo muchas personas de américa latina puedan beneficiarse y crecer profesionalmente. 
Somos una empresa reconocida legalmente; Ruc: 20609982480, Cinare Education & Consulting SAC, nuestra oficina principal se ubica en calle Juan Espinoza P5, Urb. Rosaspata, Cusco – Perú.
</p>
<h3>Afiliados Cinare</h3>
<p>¿Alguna vez has recomendado nuestros cursos a un amigo, a tus seguidores, a tus clientes o a tus alumnos? Muchas personas llegan a Cinare porque nuestros estudiantes les hablan de nuestros cursos.

El programa de afiliados es nuestra forma de agradecer y premiar esas recomendaciones que nos hacen tan felices.
Cinare es uno de los destinos para el aprendizaje y la enseñanza en línea.Hazte afiliado hoy y promociona un curso, comparte el conocimiento con personas en toda america latina. </p>
<h3>¿Cómo funciona nuestro programa de afiliados? </h3>
<p>Si eres un enamorado de Cinare y sueles recomendarnos, eres el candidato perfecto para unirte a nuestro programa de afiliados. Si todavía no nos conoces, pero estás buscando una forma de ganar dinero online, puedes hacerlo dando a conocer nuestros cursos. Solo tienes que seguir estos pasos: </p>
<h4><strong>1. Activa tu cuenta: </strong><h4>
<p>Si ya eres estudiante de Cinare, puedes darte de alta directamente desde tu área de usuario, en la sección ‘Marketing  Afiliados’. 
Si no eres estudiante de Cinare, primero tendrás que darte de alta en nuestro sistema cubriendo este formulario y posteriormente activar tu cuenta desde el área de usuario. </p>
<h4><strong>2. Promociona cursos: </strong><h4>
<p>Puedes promocionar nuestros cursos de muchas formas: en tu web, compartiendo contenidos en tus redes sociales, mediante envíos de email a tu comunidad o hablando de nosotros a conocidos que estén buscando cursos para su crecimiento profesional. 
Nos encanta cuando nuestros afiliados comparten su propia experiencia, pero también puedes probar otras ideas como publicar tutoriales, compartir trucos o usar los banners que encontrarás en tu área de usuario. </p>
<h4><strong>3. Consigue tus comisiones: </strong><h4>

<p>
Cada vez que una persona llegue a nuestra web a través de tu enlace o tus banners y se inscribe a un curso, te asignaremos una comisión del 35% del precio que pague el estudiante.
Al finalizar el periodo de garantía de 07 días de cada estudiante inscrito, se confirmará esa comisión en tu cuenta.
La liquidación de tus comisiones será a través de PayPal, cada fin de mes. También puedes convertirlas en saldo de cliente si quieres usarlas para pagar tus cursos.
No olvides indicar como quieres cobrar tus comisiones.
</p>
<h3>Beneficios por ser Afiliado en Cinare</h3>
<br/>

<h3>Genera ingresos</h3>
<p>Cada vez que una persona llegue a nuestra web a través de tu enlace o tus banners y se inscribe a un curso, te asignaremos una comisión del 35% del precio que pague el estudiante. </p>
<h3>Salarios fijos</h3>
<p>Cumple las metas y obtén un salario mensual fijo.  Las metas están basadas en acumulación de puntos, estos se acumulan por tus ventas.</p>
<h3>Capacitación constante</h3>

<p>Si no tienes conocimientos en Marketing Digital nosotros te capacitamos, accede a un curso un completo, donde aprenderás Diseño gráfico, Facebook ADS, WhatsApp Business, Cierre de ventas y mucho más. </p>

<h3>Cuenta Premium</h3>
<p>Cumple metas y accede a nuestra biblioteca digital como usuario Premium, donde podrás descargar artículos, tesis, libros, planos fotografías y mucho más.
Potencia aún más tus conocimientos con nuestra biblioteca digital. </p>
<h3>Vamos por más </h3>
<p>Invita a tus amigos o profesionales reconocidos como docente en Cinare y gana una comisión de 5% por cada estudiante inscrito de por vida.
Al confirmar y la contratación del docente, se confirmará esa comisión en tu cuenta. </p>
<h4><strong>Conviértete en Afiliado(a) en Cinare. ¡Realiza ventas online!</strong></h4>
<p>Contáctanos para recibir más información: hola@cinare.org</p>

<br/><br/>

<h1>Preguntas Frecuentes Afiliados</h1>
<br/><br/>
<div id="accordion">
    <div class="card">
      <div class="card-header" id="headingOne">
        <h5 class="mb-0">
          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <h3>¿Qué requisitos debo cumplir para ser afiliado de Cinare? </h3>
          </button>
        </h5>
      </div>
  
      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
        <p>  
            Puedes unirte a nuestro programa de afiliados siempre que seas una persona mayor de 18 años, con capacidad legal para desempeñar la actividad de afiliado.
            También puedes hacerlo si eres una empresa legalmente constituída.
            En Cinare tenemos un compromiso con la calidad y con la transparencia en cuanto a nuestras condiciones y precios.
            Este mismo compromiso es el que deben adquirir nuestros afiliados al recomendar nuestros cursos online. 
             </p> 
        </div>

      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            
        <h3> ¿Es necesario ser estudiante en Cinare para poder ser afiliado? </h3>
          </button>
        </h5>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body">

        <p> 
            No es necesario estar inscrito como estudiante para poder formar parte de nuestro programa de afiliados.
Solo tendrás que registrarte para tener acceso al área privada desde la que podrás gestionar tu participación en el programa (recursos, comisiones, etc.)
A continuación, podrás activar tu cuenta pulsando en el boton ‘Afiliate ahora’.
Aunque no es necesario ser estudiante, sí es muy recomendable. Solo llevando nuestros cursos podrás comprobar por ti mismo la calidad de nuestro trabajo y tendrás muchos más argumentos para tus recomendaciones

        </p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingThree">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        <h3> ¿Cómo y cuándo cobraré mis comisiones por Paypal? </h3>
          </button>
        </h5>
      </div>
      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
        <div class="card-body">
          
        <p> 
            Pagamos las comisiones, cada fin de mes, a través de PayPal.
            Para poder cobrar tus comisiones, solo tendrás que acumular el importe mínimo de 25$USD.
            </p>
        </div>
      </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingFour">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          <h3> No tengo cuenta de PayPal, ¿puedo cobrar de otro modo? </h3>
            </button>
          </h5>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
          <div class="card-body">
            
          <p> 
          
            El pago directo de comisiones lo hacemos únicamente vía PayPal, no realizamos pagos por otros canales. Si no tienes cuenta de PayPal, puedes crear una cuenta de forma gratuita en su sitio web.
            Si prefieres no crear una cuenta de PayPal, puedes convertir el importe de tus comisiones en saldo para tu cuenta de usuario. De esta forma podrás usar ese saldo para inscribirte en nuevos cursos con nosotros.
            
        </p>
          </div>
        </div>
      </div>
      
     
  </div>