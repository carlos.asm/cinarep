
<h2 class="mb-4" id="ensena">Enseña en Cinare</h2>
<br/>
<p>En Cinare nuestro principal objetivo es compartir el conocimiento, por ello, estas cordialmente invitado a que formes parte de este gran proyecto como docente.</p>
<p>Recuerda que todos tenemos pasión y dominio de un tema y en Cinare queremos que compartas ese conocimiento cargado de experiencia con muchas personas de américa latina. </p>
<p>
    Hazte Docente hoy y crea un curso online.</p>
    <h6><strong>¿Miedo a la Tecnología? </strong></h6>
    <p>
    Ya deja el miedo a la tecnología, hacer un curso online es más fácil de lo que crees. </p>
    <h6><strong>No sabes de qué dar el curso </strong></h6>
    <p>No te preocupes, todos tenemos pasión y dominio de un tema y ese conocimiento lo puedes compartir. </p>
    <h6><strong>¿Encontraras mucha información? </strong></h6>
    <p>Como la estructura de un curso, videos lecciones, plantillas prediseñadas, sesiones en vivo y más. </p>
    


<h4>Es Para Ti Sí eres:</h4>

<p><i class="fas fa-sort-up"></i>  Estás dando cursos presenciales o sesiones en vivo y estás cansado de intercambiar tiempo por dinero. </p></li>
<p><i class="fas fa-sort-up"></i>  Tienes pasión y dominio de una temática, tu conocimiento es muy valioso y lo deseas compartir, para ayudar a más personas.</p> </li>
<p><i class="fas fa-sort-up"></i>  Tienes mucho que compartir mediante un curso online, pero no sabes cómo empezar o peor aún, crees que el presupuesto es la 
    dificultad. 
</p>

    <h4>Beneficios por ser Docente en Cinare</h4>
    <br/>
    <h4>Genera ingresos</h4>
    <p>
Cada vez que una persona llegue a nuestra web y se inscribe a un curso, te asignaremos una comisión del 40% del precio que pague el estudiante y si llega a través de tu enlace y se inscribe a un curso, te asignaremos una comisión del 80%.
    </p><h4>Publicación de artículos científicos</h4>

    <p>Cinare tiene como propósito publicar artículos científicos y/o libros, como mecanismo de difusión de la ciencia, el docente tendrá que realizar la publicación de artículos científicos, que lleve relación con el curso o contenido publicado, Cinare proporciona todas las facilidades como; bibliografía, capacitación, material o información de primera fuente entre otros que ayude a la publicación del articulo científico por parte del docente.
    </p><h4>Escribe tu primer artículo científico </h4>
    <p>Cada año nuestros docentes reciben capacitación de expertos en metodología de investigación y redacción de artículos científicos, por lo que tendrán grandes oportunidades de escribir un artículo y crecerás profesionalmente. 
    </p><h4>Capacitación constante</h4>

    <p>Si no tienes conocimientos en Marketing Digital nosotros te capacitamos, accede a un curso un completo, donde aprenderás Diseño gráfico, Facebook ADS, WhatsApp Business, Cierre de ventas y mucho más.
    </p>
<h4>Acceso a todos los cursos</h4>

<p>Accede a todos los cursos ya activos y cursos futuros, recuerda que tu crecimiento profesional es lo primordial, por ello tendrá disfrutar y potencia aun mas con todos los cursos activos en la plataforma de Cinare.
</p>
<h4>Cuenta Premium</h4>
<p>
Cumple metas y accede a nuestra biblioteca digital como usuario Premium, donde podrás descargar artículos, tesis, libros, planos fotografías y mucho más.
Potencia aún más tus conocimientos con nuestra biblioteca digital.
</p>
<h4>Vamos por más </h4>
<p>
Invita a tus amigos o profesionales reconocidos como docente en Cinare.
</p>
