@extends(getTemplate().'.layouts.app')
@push('styles_top')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">

<style>

@media only screen and (max-width: 768px) {
  /* For mobile phones: */
  .menuleft{
    display:none;
  }

  .contenido
  {
    margin-left: .5em!important;
    max-width: 95%!important;
}
}


    .menuleft
    {
        
        position:fixed;
        min-height:800px;
    }

.menuleft h1
{
    font-size: 52px;
    line-height: 60px;
    letter-spacing: 0;   
    
}

.menuleft span {
    font-weight: 700;
    font-size: 11px;
    line-height: 16px;
    letter-spacing: 0.8px;
    text-transform: uppercase;   
}

.activo
{
    margin-top: 20px;
    border-left: solid;
    margin-left: 2em;
    color: #296dfe
}
.activo a
{
   
    margin-left: 10px;
    color: #296dfe
}

.inactivo
{
    margin-top: 20px;
    margin-left: 2em;
}
.inactivo a
{
    
    color: rgba(141,141,157,0.56);
}

.contenido
{
margin-left: 18em;
}
.contenido h2 {
    font-weight: 700;
    font-size: 32px;
    line-height: 40px;
    letter-spacing: 0;
}

.contenido h3
{
    font-weight: 700;
    font-size: 20px;
    line-height: 20px;
    margin-bottom: 16px;
}

.contenido h4
{
    margin-bottom: 10px;  
}

.contenido p
{
    margin-bottom: 25px;
    color: gray;
    text-align: justify;
}
strong{
    color:black;
}
#botontop
{
    
    font-size: 45px;
    border-radius: 15px;
    color:#296dfe;
}


</style>



@endpush


@section('content')


<div class="container" style="margin-top:45px;" id="top">
    <div class="row">
        <div class="col-md-4 menuleft">
            <h1><a href="/pages/nosotros" class="logo">Legal</a></h1>
            <span>Contenido</span>
            <ul class="list-unstyled components mb-5">

              <li class="@if($link=='nosotros') activo @else inactivo @endif item" id="liacerca">
                <a id="btnacerca" href="#">Sobre Cinare</a>
                
              </li>
              <li class="@if($link=='ensena') activo @else inactivo @endif item"  id="liensena">
                  <a id="btnensena" href="#">Enseña en Cinare</a>
              </li>
              <li class="@if($link=='afiliados') activo @else inactivo @endif item"  id="liafiliados">
              <a id="btnafiliados" href="#">Programa de Afiliados</a>
              
              </li>
              <li class="@if($link=='terminos') activo @else inactivo @endif item"  id="licondiciones">
              <a id="btncondiciones" href="#">Condiciones de uso</a>
              </li>
              <li class="@if($link=='privacidad') activo @else inactivo @endif item"  id="liprivacidad">
              <a id="btnprivacidad" href="#">Políticas de privacidad</a>
              </li>
              <li class="@if($link=='cookies') activo @else inactivo @endif item"  id="licookies">
                <a id="btncookies" href="#">Políticas de cookies</a>
                </li>
                <li class="@if($link=='propiedad') activo @else inactivo @endif item"  id="lipropiedad">
                    <a id="btnpropiedad" href="#">Propiedad intelectual</a>
                </li>

                <li class="@if($link=='faq') activo @else inactivo @endif item" " id="lifaq">
                    <a id="btnfaq" href="#">Preguntas Frecuentes</a>
                </li>
                    
            </ul>
            <br/> <br/> <br/> <br/> <br/> <br/>
        </div>
        
        <div class="contenido" style="@if($link=='nosotros') display:block; @else display:none @endif" id="acerca">            
            @include("web.default.pages.contenido.acercade")            
        </div>

        <div class="contenido" style="@if($link=='ensena') display:block; @else display:none @endif"  id="ensena">            
            @include("web.default.pages.contenido.ensena")            
        </div>

        <div class="contenido" style="@if($link=='afiliados') display:block; @else display:none @endif" id="afiliados">            
            @include("web.default.pages.contenido.afiliados")            
        </div>

        <div class="contenido" style="@if($link=='terminos') display:block; @else display:none @endif" id="condiciones">            
            @include("web.default.pages.contenido.condiciones")            
        </div>

        <div class="contenido" style="@if($link=='privacidad') display:block; @else display:none @endif" id="privacidad">            
            @include("web.default.pages.contenido.privacidad")            
        </div>

        <div class="contenido" style="@if($link=='cookies') display:block; @else display:none @endif" id="cookies">            
            @include("web.default.pages.contenido.cookies")            
        </div>

        <div class="contenido" style="@if($link=='propiedad') display:block; @else display:none @endif" id="propiedad">            
            @include("web.default.pages.contenido.propiedad")            
        </div>

        <div class="contenido" style="@if($link=='faq') display:block; @else display:none @endif" id="faq">            
            @include("web.default.pages.contenido.faq")            
        </div>
   
    </div>
    <div class="col-md-2 offset-md-10 text-right"> 
        
        <a id="botontop" href="#" ><i class="fas fa-arrow-alt-circle-up"></i>
    </a></div>
</div>

@endsection

@push('scripts_bottom')
<script>
$(document).ready(function() {
   
    $('#btnensena').click(function() {
        $('.contenido').hide();
         $('#ensena').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#liensena').removeClass('inactivo');
         $('#liensena').addClass('activo');
         
    });


    $('#btnacerca').click(function() {
        $('.contenido').hide();
         $('#acerca').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#liacerca').removeClass('inactivo');
         $('#liacerca').addClass('activo');
    });

    $('#btnafiliados').click(function() {
        $('.contenido').hide();
         $('#afiliados').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#liafiliados').removeClass('inactivo');
         $('#liafiliados').addClass('activo');
    });

    $('#btncondiciones').click(function() {
        $('.contenido').hide();
         $('#condiciones').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#licondiciones').removeClass('inactivo');
         $('#licondiciones').addClass('activo');
    });

    $('#btnprivacidad').click(function() {
        $('.contenido').hide();
         $('#privacidad').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#liprivacidad').removeClass('inactivo');
         $('#liprivacidad').addClass('activo');
    });

    $('#btncookies').click(function() {
        $('.contenido').hide();
         $('#cookies').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#licookies').removeClass('inactivo');
         $('#licookies').addClass('activo');
    });

    $('#btnpropiedad').click(function() {
        $('.contenido').hide();
         $('#propiedad').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#lipropiedad').removeClass('inactivo');
         $('#lipropiedad').addClass('activo');
    });

    $('#btnfaq').click(function() {
        $('.contenido').hide();
         $('#faq').show();
         $('.item').removeClass('activo');
         $('.item').addClass('inactivo');
         $('#lifaq').removeClass('inactivo');
         $('#lifaq').addClass('activo');
    });

});
    </script>

@endpush
