@extends(getTemplate().'.layouts.app')
@push('styles_top')

    <link rel="stylesheet" href="/acercade/css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush


@section('content')
<div class="container">
<div class="wrapper d-flex align-items-stretch">
    <nav id="sidebar">
        <div class="custom-menu">
           {{-- <button type="button" id="sidebarCollapse" class="btn btn-primary">
      <i class="fa fa-bars"></i>
      <span class="sr-only">Toggle Menu</span>
    </button>--}}
</div>
        <div class="p-4 pt-5" >
          <h1><a href="/pages/nosotros" class="logo">Legal</a></h1>
    <ul class="list-unstyled components mb-5">
      <li class="active">
        <a href="#acerca">Sobre Cinare</a>
        
      </li>
      <li>
          <a href="#ensena">Enseña en Cinare</a>
      </li>
      <li>
      <a href="#afiliados">Programa de Afiliados</a>
      
      </li>
      <li>
      <a href="#condiciones">Condiciones de uso</a>
      </li>
      <li>
      <a href="#privacidad">Políticas de privacidad</a>
      </li>
      <li>
        <a href="#cookies">Políticas de cookies</a>
        </li>
        <li>
            <a href="#propiedad">Propiedad intelectual</a>
            </li>
    </ul>



 

  </div>
</nav>

<!-- Page Content  -->
<div id="content" class="p-4 p-md-5 pt-5">

<h2 class="mb-4" id="">Sobre Cinare</h2>
<p>En Cinare el principal objetivo es compartir el conocimiento a través de los cursos online y producción académica de artículos científicos y/o libros, para de este modo muchas personas de américa latina puedan beneficiarse y crecer profesionalmente. 
    Somos una empresa reconocida legalmente; Ruc: 20609982480, Cinare Education & Consulting SAC, nuestra oficina principal se ubica en calle Juan Espinoza P5, Urb. Rosaspata, Cusco – Perú.
    </p>
    <h4>Historia</h4>
<p>Y seguramente te estarás preguntando cómo surge Cinare, ello se remonta años atrás, cuando Franklin Camala Lizaraso – Fundador de Cinare, culmina sus estudios de Arqueología en la Universidad San Antonio Abad del Cusco, como cualquier egresado, busca un trabajo, estando en la Universidad había realizado cursos especializados de algunos softwares como es AutoCAD, ya egresado, se fue dando cuenta que colegas suyos, compañeros, incluso personas que habían egresado años atrás, profesionales, no tenían conocimiento respecto al software y vio  la oportunidad de hacer un curso dirigido a ese público objetivo.
    Las primeras clases se realizaron de manera presencial en la ciudad de Cusco, Perú.
    Los resultados fueron buenos, y a la par se publicaban en las redes sociales como Facebook y a partir de este momento es donde surgen las consultas de otros departamentos incluso de otros países como México, Colombia, España donde solicitan hacer el curso en modalidad online, entonces indago más y vio que se podía lanzar el curso en modalidad online con lo que se realizó transmisiones vivo,  ya en plena pandemia, decidió dar un paso más, capacitándose aun más, llevo algunos cursos de diseño de páginas web, fue explorando y aprendiendo las redes sociales, marketing digital, grabación y edición de vídeos y entonces los cursos eran grabados y las lecciones se subían al sitio web, a medida que la comunidad crecía, profesionales de otras rubros se unieron al proyecto, para publicar nuevos cursos,  ahora los estudiantes pueden inscribirse y llevar el curso acorde a su disposición de tiempo.
    </p>

    <h4>  ¿Qué cursos puedo estudiar?</h4>
  
<p>En Cinare el principal objetivo es compartir el conocimiento a través de los cursos online y producción académica de artículos científicos y/o libros, para que muchas personas de américa latina puedan beneficiarse y crecer profesionalmente,  los cursos pueden ser en diferentes temáticas siguiendo un rigor científico como arquitectura, arqueología, antropología, artes, ciencias de la computación, ecología, educación, enfoques o corrientes teóricas, diseño gráfico, historia, humanidades, idiomas, ingenierías, metodología de la investigación, marketing digital, paleontología, programación, turismo, redacción y elaboración de artículos científicos y otros que contribuya al crecimiento profesional académico. 
Los cursos incluyen video-lecciones pregrabadas, asignaciones autocalificadas y revisadas y foros de discusión.</p>

<br/><br/>
<h2 class="mb-4" id="ensena">Enseña en Cinare</h2>
<p>En Cinare nuestro principal objetivo es compartir el conocimiento, por ello, estas cordialmente invitado a que formes parte de este gran proyecto como docente.</p>
<p>Recuerda que todos tenemos pasión y dominio de un tema y en Cinare queremos que compartas ese conocimiento cargado de experiencia con muchas personas de américa latina. </p>
<p>
    Hazte Docente hoy y crea un curso online.
    <h6><strong>¿Miedo a la Tecnología? </strong></h6>
    Ya deja el miedo a la tecnología, hacer un curso online es más fácil de lo que crees. 
    <h6><strong>No sabes de qué dar el curso </strong></h6>
    No te preocupes, todos tenemos pasión y dominio de un tema y ese conocimiento lo puedes compartir. 
    <h6><strong>¿Encontraras mucha información? </strong></h6>
    Como la estructura de un curso, videos lecciones, plantillas prediseñadas, sesiones en vivo y más. 
    
</p>

<h4>Es Para Ti Sí eres:</h4>
<ul>
<li> Estás dando cursos presenciales o sesiones en vivo y estás cansado de intercambiar tiempo por dinero. </li>
<li>Tienes pasión y dominio de una temática, tu conocimiento es muy valioso y lo deseas compartir, para ayudar a más personas. </li>
    <li>	Tienes mucho que compartir mediante un curso online, pero no sabes cómo empezar o peor aún, crees que el presupuesto es la dificultad. </li></ul>

    <h4>Beneficios por ser Docente en Cinare</h4>
    <h4>Genera ingresos</h4>
Cada vez que una persona llegue a nuestra web y se inscribe a un curso, te asignaremos una comisión del 40% del precio que pague el estudiante y si llega a través de tu enlace y se inscribe a un curso, te asignaremos una comisión del 80%.
<h4>Publicación de artículos científicos</h4>

Cinare tiene como propósito publicar artículos científicos y/o libros, como mecanismo de difusión de la ciencia, el docente tendrá que realizar la publicación de artículos científicos, que lleve relación con el curso o contenido publicado, Cinare proporciona todas las facilidades como; bibliografía, capacitación, material o información de primera fuente entre otros que ayude a la publicación del articulo científico por parte del docente.
<h4>Escribe tu primer artículo científico </h4>
Cada año nuestros docentes reciben capacitación de expertos en metodología de investigación y redacción de artículos científicos, por lo que tendrán grandes oportunidades de escribir un artículo y crecerás profesionalmente. 
<h4>Capacitación constante</h4>

Si no tienes conocimientos en Marketing Digital nosotros te capacitamos, accede a un curso un completo, donde aprenderás Diseño gráfico, Facebook ADS, WhatsApp Business, Cierre de ventas y mucho más.

<h4>Acceso a todos los cursos</h4>

Accede a todos los cursos ya activos y cursos futuros, recuerda que tu crecimiento profesional es lo primordial, por ello tendrá disfrutar y potencia aun mas con todos los cursos activos en la plataforma de Cinare.

<h4>Cuenta Premium</h4>
Cumple metas y accede a nuestra biblioteca digital como usuario Premium, donde podrás descargar artículos, tesis, libros, planos fotografías y mucho más.
Potencia aún más tus conocimientos con nuestra biblioteca digital.
<h4>Vamos por más </h4>
Invita a tus amigos o profesionales reconocidos como docente en Cinare.




<br/><br/><br/><br/>
<h2 class="mb-4" id="afiliados">Programa de Afiliados</h2>
<br/><br/><br/><br/>
<h2 class="mb-4" id="condiciones">Condiciones de uso</h2>
<br/><br/><br/><br/>
<h2 class="mb-4" id="privacidad">Políticas de privacidad</h2>
<br/><br/><br/><br/>
<h2 class="mb-4" id="cookies">Políticas de cookies</h2>
<br/><br/><br/><br/>
<h2 class="mb-4" id="propiedad">Propiedad intelectual</h2>

      


</div>


</div>
</div>
@endsection

@push('scripts_bottom')

<script src="/acercade/js/popper.js"></script>

<script src="/acercade/js/main.js"></script>
@endpush
