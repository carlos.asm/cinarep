@php
    if (!empty($itemValue) and !is_array($itemValue)) {
        $itemValue = json_decode($itemValue, true);
    }
    
@endphp
<div class="tab-pane mt-3 fade " id="referral" role="tabpanel" aria-labelledby="referral-tab">
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="tab-pane mt-3 fade active show" id="referral" role="tabpanel" aria-labelledby="referral-tab">
                <div class="row">
                   
                        <form action="/admin/settings/main" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="page" value="financial">
                            <input type="hidden" name="name" value="referral">
            
            
                            <div class="form-group custom-switches-stacked">
                                <label class="custom-switch pl-0 d-flex align-items-center">
                                    <input type="hidden" name="value[status]" value="0">
                                    <input type="checkbox" name="value[status]" id="referralStatusSwitch" value="1" checked="&quot;checked&quot;" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <label class="custom-switch-description mb-0 cursor-pointer" for="referralStatusSwitch">Active</label>
                                </label>
                                <div class="text-muted text-small mt-1">You will be able to define the platform content in different languages. If turn it off, the language dropdown will be hidden.</div>
                            </div>
            
                            <div class="form-group custom-switches-stacked">
                                <label class="custom-switch pl-0 d-flex align-items-center">
                                    <input type="hidden" name="value[users_affiliate_status]" value="0">
                                    <input type="checkbox" name="value[users_affiliate_status]" id="userReferralStatusSwitch" value="1" checked="&quot;checked&quot;" class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <label class="custom-switch-description mb-0 cursor-pointer" for="userReferralStatusSwitch">Acticate affiliate for new users by default                       </label>
                                </label>
                                <div class="text-muted text-small mt-1">By activating this toggle, the affiliate feature will be activated for new registration by default.</div>
                            </div>
           
            
                            <div class="form-group">
                                <label>Affiliate Commission</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-percentage"></i>
                                        </div>
                                    </div>
                                    <input type="number" name="value[affiliate_user_commission]" value="{{ (!empty($itemValue) and !empty($itemValue['affiliate_user_commission'])) ? $itemValue['affiliate_user_commission'] : old('affiliate_user_commission') }}" class="form-control text-center " maxlength="3" min="0" max="100">
            
                                                        </div>
            
                                <div class="text-muted text-small mt-1">The commission rate that will be awarded to the affiliate user when a purchase is made by a referred user (Percentage).</div>
                            </div>
                          
                            <div class="form-group">
                                <label>Store Affiliate Commission</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-percentage"></i>
                                        </div>
                                    </div>
                                    <input type="number" name="value[store_affiliate_user_commission]" value="{{ (!empty($itemValue) and !empty($itemValue['store_affiliate_user_commission'])) ? $itemValue['store_affiliate_user_commission'] : old('store_affiliate_user_commission') }}" class="form-control text-center " maxlength="3" min="0" max="100">
            
                                                        </div>
            
                                <div class="text-muted text-small mt-1">The commission rate that will be awarded to the affiliate user when the referred user purchases a product from the store (Percentage).</div>
                            </div>
                          
                            <div class="form-group">
                                <label>Affiliate User Registration Bonus</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-dollar-sign"></i>
                                        </div>
                                    </div>
                                    <input type="number" name="value[affiliate_user_amount]" value="{{ (!empty($itemValue) and !empty($itemValue['affiliate_user_amount'])) ? $itemValue['affiliate_user_amount'] : old('affiliate_user_amount') }}" class="form-control text-center" maxlength="8" min="0">
                                </div>
                                <div class="text-muted text-small mt-1">The fixed amount will be awarded to the affiliate user when a user signs up using the affiliate code.</div>
                            </div>
            
            
                            <div class="form-group">
                                <label>Referred User Bonus</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-dollar-sign"></i>
                                        </div>
                                    </div>
                                    <input type="number" name="value[referred_user_amount]" value="{{ (!empty($itemValue) and !empty($itemValue['referred_user_amount'])) ? $itemValue['referred_user_amount'] : old('referred_user_amount') }}" class="form-control text-center" maxlength="8" min="0">
                                </div>
                                <div class="text-muted text-small mt-1">The fixed amount will be awarded to the referred user when a user signs up using the affiliate code.</div>
                            </div>
                         
                            <div class="form-group">
                                <label>Affiliate Description</label>
                                <textarea name="value[referral_description]" class="form-control" rows="6" placeholder="">{{ (!empty($itemValue) and !empty($itemValue['referral_description'])) ? $itemValue['referral_description'] : old('referral_description')}}</textarea></div>
                            </div>
                            
                            <button type="submit" class="btn btn-success">Save Changes</button>
                        </form>
                    </div>
                  
                </div>
            </div>
        </div>
       
</div>
