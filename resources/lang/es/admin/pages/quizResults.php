<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Results Translation
    |--------------------------------------------------------------------------
    */

    'quiz_result_list_page_title' => 'Resultado de la prueba',

    'admin_quiz_result' => 'Resultado de la prueba',
    'admin_quiz_result_list' => 'Lista de resultados del cuestionario',
    'admin_quiz_result_create' => 'Resultado del cuestionario Crear',
    'admin_quiz_result_edit' => 'Resultado de la prueba Editar',
    'admin_quiz_result_delete' => 'Eliminar resultado de la prueba',
];
