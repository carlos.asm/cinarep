<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Discounts
    |--------------------------------------------------------------------------
    */

    'discount_list_page_title' => 'Lista de descuentos',
    'discount' => 'Descuento',
    'title' => 'Título',
    'tags_count' => 'Cuenta de etiquetas',
    'new_page_lead' => 'Puedes crear un nuevo descuento.',
    'parent_name' => 'Nombre del padre',
    'select_parent_name' => 'elige el nombre de los padres',
    'discount_count' => 'Las categorías cuentan',
    'page_lists_lead' => 'lista de descuento. Puede editar o eliminar cualquier fila.',
    'sub_discount' => 'descuento secundario',
    'has_sub_discount' => 'Tiene sub descuento',
    'add_sub_discount' => 'Agregar subdescuento',
    'add' => 'Agregar',
    'remove' => 'Remover',
];
