<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Categories Translation
    |--------------------------------------------------------------------------
    */

    'group_list_page_title' => 'Lista de grupos de usuarios',
    'edit_page_title' => 'Editar grupo de usuarios',
    'group' => 'Grupo',
    'title' => 'Título',
    'tags_count' => 'Cuenta de etiquetas',
    'new_page_lead' => 'Puedes crear un nuevo grupo..',
    'parent_name' => 'Nombre del padre',
    'select_parent_name' => 'elige el nombre de los padres',
    'group_count' => 'Las categorías cuentan',
    'page_lists_lead' => 'Lista de grupo. Puede editar o eliminar cualquier fila.',
    'sub_group' => 'subgrupo',
    'has_sub_group' => 'Tiene subgrupo',
    'add_sub_group' => 'Agregar subgrupo',
    'add' => 'Agregar',
    'remove' => 'Remover',

    'admin_group' => 'Grupos',
    'admin_group_list' => 'Lista de grupos',
    'admin_group_create' => 'Grupos Crear',
    'admin_group_edit' => 'Grupos Editar',
    'admin_group_delete' => 'Grupos Eliminar',
];
