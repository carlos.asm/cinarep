<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Webinars Translation
    |--------------------------------------------------------------------------
    */

    'quizzes' => 'Cuestionarios',
    'page_lists_lead' => 'Lista de cuestionarios. Puede editar o eliminar cualquier fila.',
    'page_results_lead' => 'Ver resultados de la prueba',
    'title' => 'Título',
    'instructors' => 'Instructores',
    'instructor' => 'Instructor',
    'quizzes_list' => 'Listas de cuestionarios',
    'quiz_results' => 'Resultados de cuestionarios',
    'question_count' => 'Recuento de preguntas',
    'students_count' => 'los estudiantes cuentan',
    'average_grade' => 'Nota media',
    'grades' => 'Los grados',
    'grade_date' => 'Grados Fecha',
    'certificate' => 'Certificado',

    'passed' => 'Pasó',

    'certificates' => 'Certificados',
    'certificates_templates' => 'Plantillas de certificados',
    'certificates_lists_lead' => 'Consulte la lista de certificados descargados. también puedes descargar',
    'certificates_templates_lists_lead' => 'Ver la lista de plantilla de certificados',
    'templates' => 'Plantillas',
    'new_template' => 'Nueva plantilla',

    'template_image' => 'Imagen de plantilla',

    'position_x' => 'Posicion x',
    'position_y' => 'Posicion y',
    'font_size' => 'Tamaño de fuente',
    'text_color' => 'Color de texto',
    'message_body' => 'Cuerpo del mensaje',
    'preview_certificate' => 'Vista previa del certificado',

    'admin_quizzes' => 'Cuestionarios',
    'admin_quizzes_list' => 'Lista de cuestionarios',
    'admin_quizzes_create' => 'Cuestionarios Crear',
    'admin_quizzes_edit' => 'Cuestionarios Editar',
    'admin_quizzes_delete' => 'cuestionarios Eliminar',
];
