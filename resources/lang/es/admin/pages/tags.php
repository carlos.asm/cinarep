<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Tags Translation
    |--------------------------------------------------------------------------
    */

    'tags_list_page_title' => 'Lista de etiquetas',
    'tag' => 'Etiqueta',
    'tags' => 'Etiquetas',
    'title' => 'Título',
    'create_field_title_placeholder' => 'elige un titulo.',
    'tags_count' => 'conteo de etiquetas',
    'new_page_lead' => 'Puedes crear una nueva etiqueta.',
    'page_lists_lead' => 'Lista de etiquetas. Puede editar o eliminar cualquier fila.',

    'admin_tags' => 'Etiquetas',
    'admin_tags_list' => 'Lista de etiquetas',
    'admin_tags_create' => 'Etiquetas Crear',
    'admin_tags_edit' => 'Etiquetas Editar',
    'admin_tags_delete' => 'Etiquetas Eliminar',
];
