<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Payment Gateways
    |--------------------------------------------------------------------------
    */

    'payment_channels' => 'Canales de pago',
    'payment_channel' => 'Canal de pago',
    'payment_channel_edit' => 'Editar canal de pago',
    'title' => 'Título',
    'tags_count' => 'Cuenta de etiquetas',
    'new_page_lead' => 'Puede crear un nuevo canal de pago.',
    'parent_name' => 'Nombre del padre',
    'select_parent_name' => 'elige el nombre de los padres',
    'payment_channel_count' => 'Las categorías cuentan
',
    'page_lists_lead' => 'Lista de canales_pago. Puede editar o eliminar cualquier fila.',
    'sub_payment_channel' => 'Canal_de_pago secundario',
    'has_sub_payment_channel' => 'Tiene un canal_de_pago secundario',
    'add_sub_payment_channel' => 'Agregar subcanal_de_pago',
    'add' => 'Agregar',
    'remove' => 'Remover',

    'admin_payment_channel' => 'Canales de pago',
    'admin_payment_channel_list' => 'Lista de canales de pago',
    'admin_payment_channel_create' => 'Canales de Pago Crear',
    'admin_payment_channel_edit' => 'Canales de pago Editar',
    'admin_payment_channel_delete' => 'Eliminar canales de pago',
];
