<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin User Roles Translation
    |--------------------------------------------------------------------------
    */

    'role' => 'Rol',
    'roles' => 'Roles',
    'select_sections_level' => 'Seleccionar nivel de secciones',
    'page_lists_title' => 'Roles del usuario',
    'page_lists_lead' => 'Lista de roles. Puede editar o eliminar cualquier fila.',
    'new_page_title' => 'Nuevo rol',

    'sections' => 'Secciones',
    'new_page_lead' => 'Puedes crear un nuevo rol.',
    'select_type' => 'Seleccione un tipo',

    'create_field_name_placeholder' => 'ejemplo: administrador',
    'is_admin' => 'Acceso al panel de administración',

    'admin_roles' => 'Roles',
    'admin_roles_list' => 'Lista de funciones',
    'admin_roles_create' => 'Roles Crear',
    'admin_roles_edit' => 'Roles Editar',
    'admin_roles_delete' => 'Eliminar roles',

];
