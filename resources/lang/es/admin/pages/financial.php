<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Financial Translation
    |--------------------------------------------------------------------------
    */

    'financial' => 'Financiera',
    'accounting' => 'Contabilidad',
    'discount' => 'descuento',
    'discounts' => 'descuentos',
    'discount_code' => 'código de descuento',
    'discount_codes' => 'Códigos de descuento',
    'name' => 'Nombre',
    'count' => 'Contar',
    'name_placeholder' => 'Elección Nombre único',
    'count_placeholder' => 'Máximo de veces que se puede utilizar el código de descuento (por una vez utilizable llévelo vacío)',
    'expiration' => 'Vencimiento',
    'ext_date' => 'Fecha de caducidad',
    'percent' => 'Por ciento',
    'amount' => 'Monto',
    'amount_placeholder' => 'Importe máximo del código de descuento para un pedido',

    'payout' => 'Pagar',
    'payouts' => 'Pagos',
    'account' => 'Cuenta',
    'type' => 'Escribe',
    'id' => 'Identificación',
    'iban' => 'IBAN',

    'sale' => 'Rebaja',
    'sales' => 'Rebaja',
    'sales_list' => 'Lista de ventas',
    'sale_type' => 'Tipo de venta',
    'file' => 'Expediente',
    'meeting' => 'Reunión',
    'ticket' => 'Boleto',
    'seller' => 'Vendedor',
    'buyer' => 'Comprador',
    'waiting_payment' => 'A la espera del pago',
    'delivered' => 'Entregado',
    'paid_failed' => 'Pagado/Fallido',
    'paid_waiting' => 'Pagado/Esperando',
    'paid_successful' => 'Pagado/Exitoso',

    'offline_payment' => 'Pago fuera de línea',
    'offline_payments' => 'Pagos fuera de línea',
    'bank' => 'Banco',
    'referral_code' => 'Código de referencia',

    'documents' => 'Documentos',
    'new_document' => 'Nuevo documento',
    'addiction' => 'Adiccion',
    'deduction' => 'Deducción',
    'creator' => 'Creador',
    'automatic' => 'Automatico',
    'target_account' => 'Cuenta objetivo',
    'income' => 'Ingreso',
    'user_income' => 'Ingresos del usuario',
    'account_balance' => 'Saldo de la cuenta',

    'factor' => 'Factor',
    'new_factor' => 'Nuevo Factor',

    'sales_page_title' => 'Ventas',
    'webinar' => 'seminario web',
    'type_account' => 'Tipo Cuenta',
    'item_purchased' => 'Articulo comprado',
    'asset' => 'Activo',
    'document_page_title' => 'Documentos',
    'system' => 'Sistema',
    'tax' => 'Impuesto',

    'new_document_page_title' => 'Nuevo Documento',
    'bank_name' => 'Nombre del banco',
    'account_name' => 'Nombre de la cuenta',
    'account_number' => 'Número de cuenta',

    'verify_user' => 'Verificar usuario',
    'financial_approval' => 'Aprobación financiera',
    'user_commission' => 'Comisión de usuario',
    'commission' => 'comisión',
    'user_commission_placeholder' => 'vacío significa usar desde la configuración predeterminada',

    'offline_payment_page_title' => 'Pago fuera de línea',
    'refund' => 'Reembolso',

    'manual_document' => 'documento manual',
    'automatic_document' => 'documento automático',

    'new_subscribe' => 'nueva suscripción',
    'purchased_subscribe' => 'Suscripción comprada',
    'purchased_promotion' => 'promoción comprada',
    'subscribe' => 'Suscribir',
    'subscribes' => 'se suscribe',
    'subscribes_lists_page_lead' => 'Lista de suscriptores. Puede editar o eliminar cualquier fila.',
    'usable_count' => 'Recuento utilizable',
    'short_description' => 'Breve descripción',
    'short_description_placeholder' => 'Me gusta: Sugerido para pequeñas empresas.',

    'promotion' => 'Promoción',
    'promotions' => 'Promociones',
    'promotions_lists_page_lead' => 'Lista de promociones. Puede editar o eliminar cualquier fila.',
    'is_popular' => 'Insignia popular',
    'new_promotion' => 'Nuevo Plan de Promoción',
    'edit_promotion' => 'Editar plan de promoción',

    'payment_channels' => 'Canales de pago',
    'promotion_sales' => 'Ventas promocionales',
    'promotion_sales_lists_page_lead' => 'Lista de Ventas en Promoción. Puedes ver.',
    'special_offers' => 'Ofertas especiales',
    'special_offers_list_page_title' => 'Ofertas especiales',
    'status' => 'Estado',
    'from_date' => 'Partir de la fecha',
    'to_date' => 'Hasta la fecha',
    'document_number' => 'Identificación del documento'
];
