<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Webinars Translation
    |--------------------------------------------------------------------------
    */

    'webinars_list_page_title' => 'lista de clases',
    'webinar' => 'seminario web',
    'webinars' => 'seminarios web',
    'courses' => 'Cursos',
    'live_classes' => 'Clases en vivo',
    'text_courses' => 'Cursos de texto',
    'title' => 'Título',
    'webinar_title' => 'título del seminario web',
    'webinar_status' => 'Estado del seminario web',
    'start_date' => 'Start Date',
    'end_date' => 'Fecha final',
    'capacity' => 'Capacidad',
    'price' => 'Precio',
    'description' => 'Descripción',
    'status' => 'Estado',
    'course_type' => 'Tipo de curso',
    'type_webinar' => 'seminario web',
    'type_text_lesson' => 'lección adicional',
    'type_course' => 'Curso',
    'type_webinars' => 'seminarios web',
    'type_text_lessons' => 'Lecciones de texto',
    'type_courses' => 'Cursos',
    'teacher_name' => 'Nombre del maestro',
    'student' => 'Alumno',
    'category_name' => 'nombre de la categoría',
    'select_category' => 'selecciona una categoría',
    'select_teacher' => 'elegir un nombre de profesor',
    'select_status' => 'elige un estado',
    'webinars_count' => 'Los seminarios web cuentan',
    'image_cover' => 'Portada de la imagen',
    'choose_image_cover' => 'elige la portada de la imagen',
    'choose_video_demo' => 'elegir vídeo de demostración',
    'video_demo' => 'Vídeo de demostración',
    'support' => 'Apoyo',
    'partner_instructor' => 'Instructor asociado',
    'subscribe' => 'Suscribir',
    'create_field_title_placeholder' => '.',
    'new_page_lead' => 'Puede crear un nuevo seminario web.',
    'page_lists_lead' => 'lista de seminarios web. Puede editar o eliminar cualquier fila.',
    'search_webinar' => 'Buscar seminario web',

    'admin_webinars' => 'seminarios web',
    'admin_webinars_list' => 'Lista de seminarios web',
    'admin_webinars_create' => 'Crear seminarios web',
    'admin_webinars_edit' => 'Seminarios web Editar',
    'admin_webinars_delete' => 'Eliminar seminarios web',

    'feature_webinars' => 'Clases destacadas',
    'feature_webinar_create' => 'crear seminario web de características',
    'page' => 'Página',
    'select_page' => 'seleccionar pagina',
    'page_home' => 'hogar',
    'page_categories' => 'Categorias',
    'page_home_categories' => 'Inicio y categorías',

    'sale_count' => 'Recuento de ventas',
    'file_sale_count' => 'ventas de archivos',
    'session_sale_count' => 'Ventas de sesiones',
    'lesson_sale_count' => 'Ventas de sesiones',

    'not_conducted' => 'no realizado',
    'conducted' => 'Realizado',

    'webinars_reports' => 'Informes de seminarios web',
    'webinars_reports_lists_lead' => 'Lista de informes de seminarios web. Puede editar o eliminar cualquier fila.',

];
