<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Filters
    |--------------------------------------------------------------------------
    */

    'filters' => 'filtros',
    'new_page_lead' => 'Puedes crear un nuevo filtro..',
    'edit_page_lead' => 'Puedes editar este filtro.',

    'add_options' => 'Agregar opciones',
    'option' => 'Opción',
    'filter_option' => 'opción de filtro',

    'tags_list_page_title' => 'Lista de filtros',
    'filter' => 'Etiqueta',
    'create_field_title_placeholder' => 'elige un titulo.',
    'create_field_title_placeholder' => 'elige un titulo.',
    'filters_count' => 'Los filtros cuentan',
    'page_lists_lead' => 'Lista de filtros. Puede editar o eliminar cualquier fila.',

    'admin_filters' => 'filtros',
    'admin_filters_list' => 'Lista de filtros',
    'admin_filters_create' => 'Filtros Crear',
    'admin_filters_edit' => 'Filtros Editar',
    'admin_filters_delete' => 'Eliminar filtros',

];
