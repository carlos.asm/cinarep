<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Categories Translation
    |--------------------------------------------------------------------------
    */

    'categories_list_page_title' => 'Lista de categorías',
    'category' => 'Categoría',
    'categories' => 'Categoría',
    'choose_category' => 'Seleccione una categoría',
    'name' => 'Nombre',
    'title' => 'Título',
    'tags_count' => 'Etiquetas',
    'new_page_lead' => 'Puedes crear una nueva categoría.',
    'parent_name' => 'Nombre del padre',
    'select_parent_name' => 'elige el nombre de los padres',
    'categories_count' => 'Las categorías cuentan',
    'page_lists_lead' => 'Lista de categorías. Puede editar o eliminar cualquier fila.',
    'trends_lists_lead' => 'Lista de categorías de tendencias. Puede editar o eliminar cualquier fila.',
    'sub_category' => 'Subcategoría',
    'has_sub_category' => 'Tiene subcategorías',
    'add_sub_categories' => 'Añadir subcategorías',
    'add' => 'Agregar',
    'remove' => 'Remover',

    'admin_categories' => 'Categorias',
    'admin_categories_list' => 'Lista de categorías',
    'admin_categories_create' => 'Categorías Crear',
    'admin_categories_edit' => 'Categorías Editar',
    'admin_categories_delete' => 'Categorías Eliminar',

    'trends' => 'Tendencias',
    'new_trend' => 'Nueva tendencia',
    'trend_icon' => 'Icono de tendencia',
    'trend_color' => 'Color de tendencia',
    'trend_color_placeholder' => 'me gusta: #3dbca7 or rgb(0,0,0)',
    'create_trend_category' => 'Crear categoría de tendencia',
];
