<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Blog Translation
    |--------------------------------------------------------------------------
    */

    'classes_comments' => 'Clases Comentarios',
    'blog_comments' => 'Comentarios del blog',
    'comments' => 'Comentarios',
    'comment' => 'comentario',
    'post' => 'Correo',
    'post_or_webinar' => 'Publicación o seminario web',
    'enable_comments' => 'Habilitar comentarios',
    'publish_item' => 'Publicar elemento',
    'content' => 'Contenido',
    'content_placeholder' => 'Contenido principal',
    'description_placeholder' => 'elige una breve descripción',
    'reply' => 'Respuesta',
    'replied' => 'Respondido',
    'comment_type' => 'tipo de comentario',
    'main_comment' => 'comentario principal',
    'reply_comment' => 'Responder comentario',
    'reply_comments' => 'Responder comentarios',
    'reply_list' => 'lista de respuestas',
    'edit_comment' => 'Editar comentario',

    'reports' => 'reports',
    'classes_comments_reports' => 'Comentarios de clase',
    'classes_reports' => 'Informes de clase',
    'blog_comments_reports' => 'Informes de comentarios de blog',
    'comments_reports' => 'Informes de comentarios',
    'reported_comment' => 'comentario denunciado',
    'report_detail' => 'Detalles del informe',
    'report_message' => 'Informar mensaje',
    'comments_lists_lead' => 'Lista de comentarios del blog. Puede editar o eliminar cualquier fila.',
    'comments_reports_lists_lead' => 'Lista de informes de comentarios. Puede editar o eliminar cualquier fila.',
    'author' => 'Autor',

    'testimonials' => 'Testimonios',
    'new_testimonial' => 'Nuevo testimonio',
    'edit_testimonial' => 'Editar testimonio',
    'testimonials_lists_lead' => 'Lista de comentarios de testimonios. Puede editar o eliminar cualquier fila.',
    'testimonial_user_avatar_placeholder' => 'vacío significa usar el avatar predeterminado de los usuarios',
    'testimonial_rate_placeholder' => 'número entre 0 a 5',
];
