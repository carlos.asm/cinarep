<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Certificates Translation
    |--------------------------------------------------------------------------
    */

    'certificate_list_page_title' => 'Lista de certificados',
    'certificate' => 'Certificado',
    'title' => 'Título',
    'tags_count' => 'Cuenta de etiquetas',
    'new_page_lead' => 'Puede crear un nuevo certificado.',
    'parent_name' => 'Nombre del padre',
    'select_parent_name' => 'elige el nombre de los padres',
    'certificate_count' => 'elige el nombre de los padres',
    'page_lists_lead' => 'Lista de certificados. Puede editar o eliminar cualquier fila.',
    'sub_certificate' => 'Subcertificado',
    'has_sub_certificate' => 'Tiene subcertificado',
    'add_sub_certificate' => 'Añadir subcertificado',
    'add' => 'Agregar',
    'remove' => 'Remover',

    'admin_certificate' => 'Certificado',
    'admin_certificate_list' => 'Lista de certificados',
    'admin_certificate_create' => 'Crear certificado',
    'admin_certificate_edit' => 'Certificado Editar',
    'admin_certificate_delete' => 'Eliminar certificado',
];
